﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Web.Helpers;
using System.Collections.ObjectModel;
using System.Security.Cryptography;
using System.IO;
using System.Windows.Media.Imaging;
using System.Threading;

namespace BooruGrabber.Util
{
    public sealed class ProxyHelper
    {
        private static ProxyHelper instance;
        private int currentIndex = 0;
        private List<Proxy> proxy = new List<Proxy>();

        private Object proxyLocker = new Object();

        public static ProxyHelper ProxyList
        {
            get
            {
                if (instance == null)
                {
                    instance = new ProxyHelper();
                }
                return instance;
            }
        }

        private ProxyHelper()
        {

        }

        public int LoadProxies(string proxyList, bool needCheck = false)
        {
            return LoadProxies(proxyList.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries).Select(a => a.Split(' ')[0].Trim()).ToArray(), needCheck);
        }

        public int LoadProxies(Stream proxyList, bool needCheck = false)
        {
            string str = (new StreamReader(proxyList)).ReadToEnd();
            return LoadProxies(str, needCheck);
        }

        public int LoadProxies(string[] proxyList, bool needCheck = false)
        {
            try
            {
                var v = proxyList.Select(a =>
                {
                    try
                    {
                        var pp = a.Split(':');
                        if (pp.Count() != 2)
                            return Proxy.Empty;

                        //сюда должен передаваться массив строк в формате IP:PORT. Поэтому пытаемся распарсить как айпишник
                        IPAddress ip;
                        bool result = IPAddress.TryParse(pp[0], out ip);
                        if (!result)
                            return Proxy.Empty;

                        int port;
                        result = int.TryParse(pp[1], out port);
                        if (!result)
                            return Proxy.Empty;

                        return new Proxy() { proxyAddress = pp[0], port = port };
                    }
                    catch
                    {
                        return Proxy.Empty;
                    }
                }).Where(a => a != Proxy.Empty).AsParallel().Select<Proxy, Proxy>(a =>
                {
                    if (!needCheck)
                        return a;
                    HttpWebRequest wr = WebRequest.CreateHttp("http://www.google.com");     //он же не должен падать, не правда ли?
                    wr.UserAgent = "Other";
                    wr.Timeout = 3000;      //по дефолту там секунд 30, а зачем нам столько ждать? Да и такая медленная прокся не сможет адекватно пикчи качать всё ранво
                    wr.Proxy = new WebProxy(a.proxyAddress, a.port);
                    try
                    {
                        wr.GetResponse();       //если прокся не работает - то вывалимся в эксепшн
                        return a;
                    }
                    catch (WebException)
                    {
                        return Proxy.Empty;
                    }
                }).Where(a => a != Proxy.Empty).ToArray();
                lock (proxyLocker)
                {
                    proxy.AddRange(v.Except(proxy));        //не вставляем повторы
                }

                return v.Count();
            }
            catch       //плевать, что за ошибка - суть в том, что мы прокси не загрузили
            {
                return -1;
            }
        }

        public Proxy GetProxy()
        {
            if (proxy.Count == 0)
                //throw new ProxyListIsEmptyException();
                return null;
            Proxy val;
            lock (proxyLocker)
            {
                if (proxy.Count <= ++currentIndex)
                    currentIndex = 0;
                val = proxy[currentIndex];
            }

            return val;
        }

        public void ReportFailedProxy(Proxy p)
        {
            if (p == null) return;
            p.failCount++;
            if (p.failCount >= 5)
                proxy.Remove(p);
        }

        public void ReportSucceededProxy(Proxy p)
        {
            if (p == null) return;
            p.failCount = 0;
        }

        public class Proxy
        {
            public string proxyAddress { get; set; }
            public int port { get; set; }
            public int failCount { get; set; }

            private static Proxy _empty = new Proxy() { proxyAddress = null, port = 0 };
            public static Proxy Empty
            {
                get
                {
                    return _empty;
                }
            }

            public override bool Equals(object obj)
            {
                if (!(obj is Proxy))
                    return base.Equals(obj);
                Proxy p = obj as Proxy;
                return (this.proxyAddress == p.proxyAddress && this.port == p.port) ? true : false;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            public static bool operator ==(Proxy left, Proxy right)
            {
                if ((object)left == null && (object)right == null) return true;
                if ((object)left == null && (object)right != null) return false;
                return left.Equals(right);
            }

            public static bool operator !=(Proxy left, Proxy right)
            {
                return !(left == right);
            }
        }
    }
}
