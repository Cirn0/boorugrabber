﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.ObjectModel;

namespace BooruGrabber.Util
{
    public static class ThreadingUtil
    {
        private static int tasksCounter = 0;

        public static async Task StartNew(object param, int threadsCount, Action<object> lambda)
        {
            if (tasksCounter != 0) throw new Exception("В данный момент уже выполняется какая-то операция");
            if (threadsCount < 1) throw new Exception("Количество потоков должно быть больше 0");

            if (threadsCount > 1)
            {
                for (int i = 0; i < threadsCount; i++)
                {
                    StartNewSubtask(param, lambda);
                    tasksCounter++;
                }
                await Task.Factory.StartNew(() =>
                {
                    while (tasksCounter > 0)
                    {
                        Thread.Sleep(1000);
                    }
                });
            }
            else        //если мы будем выполнять в один поток, то можно избавиться от ненужных действий и не тратить время
            {
                await Task.Factory.StartNew(() =>
                {
                    lambda(param);
                });
            }
        }

        private static void StartNewSubtask(object param, Action<object> lambda)
        {
            Thread thr = new Thread(new ParameterizedThreadStart(lambda));
            thr.IsBackground = true;
            thr.Start(param);

            Task.Factory.StartNew(() =>
            {
                while (thr.ThreadState != ThreadState.Stopped)
                {
                    Thread.Sleep(250);
                }
                tasksCounter--;
            });
        }
    }
}
