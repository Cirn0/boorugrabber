﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooruGrabber.Util
{
    public static class TagCorrector
    {
        public static string CorrectLink(string link)
        {
            return link.Replace("%", "%25")
                       .Replace(":", "%3A")
                       .Replace(";", "%3B")
                       .Replace("@", "%40")
                       .Replace("!", "%21")
                       .Replace("#", "%23")
                       .Replace("&", "%26")
                       .Replace("'", "%27")
                       .Replace(" ", "%20");
        }
    }
}
