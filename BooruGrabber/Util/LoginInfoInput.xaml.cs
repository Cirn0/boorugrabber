﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Security;

namespace BooruGrabber.Util
{
    /// <summary>
    /// Interaction logic for LoginInfoInput.xaml
    /// </summary>
    public partial class LoginInfoInput : Window
    {
        bool isEdited = false;
        private LoginInfoInput()
        {
            InitializeComponent();
        }

        public static Tuple<string, SecureString> GetLoginAndPassword(Tuple<string, SecureString> prevState, string windowHeader)
        {
            var form = new LoginInfoInput();
            form.Title = windowHeader;
            string login = "";
            SecureString pass = null;
            if (prevState != null)
            {
                pass = prevState.Item2;
                login = prevState.Item1;
                form.textBox.Text = login;
                form.passwordBox.Password = "Empty";
                form.passwordBox.TextInput += (_, __) => 
                {
                    form.isEdited = true;
                };
                form.textBox.TextInput += (_, __) =>
                {
                    form.isEdited = true;
                };
            }
            else
            {
                form.isEdited = true;
            }
            form.Closing += (_, __) =>
            {
                if (form.isEdited)
                {
                    login = form.textBox.Text;
                    pass = form.passwordBox.SecurePassword;
                }
                else
                {
                    //Ничего не меняем. Нужные значения уже записаны в эти переменные
                }
            };

            bool? b = form.ShowDialog();
            b = (b == null) ? false : b;

            return ((bool)b) ? new Tuple<string, SecureString>(login, pass) : null;
        }

        private void button_Copy_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            RegistryHelper.WriteWindowState("loginInfo", new RegistryHelper.WindowState() { height = this.Height, left = this.Left, top = this.Top, width = this.Width });
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            var v = RegistryHelper.ReadWindowStateValue("loginInfo");
            if (v == null)
            {
                return;
            }
            this.Height = v.height;
            this.Width = v.width;
            this.Left = v.left;
            this.Top = v.top;
        }
    }
}
