﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using BooruGrabber.Util.GrabbedData;
using BooruGrabber.Util.Grabber;

namespace BooruGrabber.Util
{
    public static class GrabberUtil
    {
        private static Tuple<string, byte[]> prevLoginProtectedState;
        private static List<string> hashedFolderContent = new List<string>();
        private static List<string> tempToHash = null;
        private static Type prevHashAlgo = null;
        private static bool hashed = false;

        public static AbstractGrabber GetGrabberBySiteName(string siteName)
        {
            DownloadingSettings.settings.Reset();
            switch (siteName)
            {
                case "Safebooru":
                    {
                        return new SafebooruGrabber();
                    }
                //break;
                case "Konachan":
                    {
                        DownloadingSettings.settings.allowSaveTagsInFileName = false;        //там теги по умолчанию указаны в имени файла, так что галка работать не будет
                        return new KonachanGrabber();
                    }
                //break;
                case "TBIB":
                    {
                        return new TBIBGrabber();
                    }
                //break;
                case "Yandere":
                    {
                        DownloadingSettings.settings.allowSaveTagsInFileName = false;        //аналогично коначану
                        return new YandereGrabber();
                    }
                //break;
                case "Sankaku":
                    {
                        Tuple<string, SecureString> result = DataProtection.ReadCredentialsFromRegistry(siteName);
                        try
                        {

                            if (result == null)
                            {
                                if (prevLoginProtectedState != null)
                                {
                                    SecureString ss = new SecureString();
                                    byte[] pass = ProtectedData.Unprotect(prevLoginProtectedState.Item2, null, DataProtectionScope.CurrentUser);
                                    for (int i = 0; i < pass.Length; i++)
                                        ss.AppendChar((char)pass[i]);
                                    DataProtection.ClearByteArray(ref pass);
                                    result = LoginInfoInput.GetLoginAndPassword(new Tuple<string, SecureString>(prevLoginProtectedState.Item1, ss), siteName);
                                    ss.Clear();
                                }
                                else
                                {
                                    result = LoginInfoInput.GetLoginAndPassword(null, siteName);
                                }

                                if (result != null)
                                {
                                    prevLoginProtectedState = new Tuple<string, byte[]>(result.Item1, ProtectedData.Protect(DataProtection.ReadDataFromSecureString(result.Item2), null, DataProtectionScope.CurrentUser));
                                }
                            }
                            if (result == null) return null;
                        }
                        catch
                        {
                            Log.log.AddEntry(Log.MessageType.warning, String.Format("Ошибка при обработке логина/пароля."));
                        }
                        //prevLoginState = v;
                        return new SankakuGrabber(result.Item1, result.Item2);
                    }
                //break;
                case "TheDoujin":
                    {
                        DownloadingSettings.settings.allowSaveTagsInFileName = false;
                        return new TheDoujinGrabber();
                    }
                //break;
                case "Rule34xxx":
                    {
                        return new Rule34xxxGrabber();
                    }
                //break;
                case "Behoimi":
                    {
                        return new BehoimiGrabber();
                    }
                //break;
                case "Danbooru":
                    {
                        return new DanbooruGrabber();
                    }
                //break;
                case "Gelbooru":
                    {
                        return new GelbooruGrabber();
                    }
                //break;
                case "Hijiribe":
                    {
                        return new HijiribeGrabber();
                    }
                //break;
                case "Sonohara":
                    {
                        return new SonoharaGrabber();
                    }
                //break;
                case "E621":
                    {
                        return new E621Grabber();
                    }
                //break;
                case "Zerochan":
                    {
                        return new ZerochanGrabber();
                    }
                //break;
                case "DeviantArt":
                    {
                        return new DeviantArtGrabber();
                    }
                //break;
                case "Minitokyo":
                    {
                        Tuple<string, SecureString> result = DataProtection.ReadCredentialsFromRegistry(siteName);
                        try
                        {
                            if (result == null)
                            {
                                if (prevLoginProtectedState != null)
                                {
                                    SecureString ss = new SecureString();
                                    byte[] pass = ProtectedData.Unprotect(prevLoginProtectedState.Item2, null, DataProtectionScope.CurrentUser);
                                    for (int i = 0; i < pass.Length; i++)
                                        ss.AppendChar((char)pass[i]);
                                    DataProtection.ClearByteArray(ref pass);
                                    result = LoginInfoInput.GetLoginAndPassword(new Tuple<string, SecureString>(prevLoginProtectedState.Item1, ss), siteName);
                                    ss.Clear();
                                }
                                else
                                {
                                    result = LoginInfoInput.GetLoginAndPassword(null, siteName);
                                }

                                if (result != null)
                                {
                                    prevLoginProtectedState = new Tuple<string, byte[]>(result.Item1, ProtectedData.Protect(DataProtection.ReadDataFromSecureString(result.Item2), null, DataProtectionScope.CurrentUser));
                                }
                            }
                            if (result == null)
                            {
                                //Сайт не показывает список тегов, если ты не залогинен. Так что в этом случае мы их никак не узнаем
                                DownloadingSettings.settings.allowSaveTagsInFileName = false;
                                return new MinitokyoGrabber();
                            }
                        }
                        catch
                        {
                            Log.log.AddEntry(Log.MessageType.warning, String.Format("Ошибка при обработке логина/пароля."));
                        }

                        var grab = new MinitokyoGrabber();
                        grab.UpdateCookie(result.Item1, result.Item2);
                        result.Item2.Clear();
                        return grab;
                    }
                    //break;
            }
            return null;
        }

        public static async Task SkipDoubles(string path, List<SimpleGrabbedFile> filesToDownload, HashAlgorithm hashAlgorithm)
        {
            await Task.Factory.StartNew(() => 
            {
                hashAlgorithm = hashAlgorithm ?? new MD5CryptoServiceProvider();
                if (!((prevHashAlgo?.Equals(hashAlgorithm.GetType()) ?? false) && hashed))
                {
                    DirectoryInfo di = new DirectoryInfo(path);
                    List<FileInfo> enumeratedFiles = di.EnumerateFiles().ToList();
                    enumeratedFiles.ForEach(a => 
                    {
                        using (Stream file = new FileStream(a.FullName, FileMode.Open))
                        {
                            string hash = BitConverter.ToString(hashAlgorithm.ComputeHash(file)).Replace("-", "").ToLowerInvariant();
                            hashedFolderContent.Add(hash);
                        }
                    });
                }

                List<SimpleGrabbedFile> toRemove = new List<SimpleGrabbedFile>();
                filesToDownload.Select(a => (hashedFolderContent.Contains(a.hash.ToLowerInvariant()) ? a : null)).Where(a => a != null).ToList().ForEach(a => toRemove.Add(a));
                toRemove.ForEach(a => filesToDownload.Remove(a));

                prevHashAlgo = hashAlgorithm.GetType();
            });
        }

        public static bool isHashFound(string hash)
        {
            return hashedFolderContent.Contains(hash);
        }
    }
}
