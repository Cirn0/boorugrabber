﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace BooruGrabber.Util
{
    public static class DataProtection
    {
        public static byte[] ReadDataFromSecureString(SecureString pass)
        {
            IntPtr bstr = IntPtr.Zero;
            short[] arr = new short[pass.Length];       //почему шорт, а не байт, ведь потом всё равно приводим к байту? Была проблема в том, что если я беру тип байт и пытаюсь сделать Marshal.Copy в этот массив, то правильные цифры у меня перемешаны с нулями (что-то типа 49 0 64 0 57 0 51 0 и так далее). Я так и не понял, с чем это связано, а ковыряться не было желания. Поэтому и сделал так, через задницу. Но на моём компьютере всё в итоге работает
            byte[] result = new byte[pass.Length];
            try
            {
                bstr = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(pass);
                System.Runtime.InteropServices.Marshal.Copy(bstr, arr, 0, pass.Length);
                for (int i = 0; i < arr.Length; i++)
                {
                    result[i] = (byte)arr[i];
                }
            }
            finally
            {
                if (bstr != IntPtr.Zero)
                {
                    System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(bstr);
                }
                for (int i = 0; i < arr.Length; i++)
                {
                    arr[i] = 0;
                }
            }

            return result;
        }

        public static void ClearByteArray(ref byte[] array)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(array);
        }

        public static void StoreCredentialsInRegistry(string _siteName, string _login, SecureString _pass)
        {
            string siteTag = _siteName;

            byte[] pass = ProtectedData.Protect(DataProtection.ReadDataFromSecureString(_pass), Encoding.UTF8.GetBytes(siteTag), DataProtectionScope.CurrentUser);
            byte[] login = ProtectedData.Protect(Encoding.UTF8.GetBytes(_login), Encoding.UTF8.GetBytes(siteTag), DataProtectionScope.CurrentUser);

            string result = Convert.ToBase64String(login) + " " + Convert.ToBase64String(pass);

            RegistryHelper.WriteCredentialValue(siteTag, result);
        }

        public static Tuple<string, SecureString> ReadCredentialsFromRegistry(string _siteName)
        {
            string valueFromRegistry = RegistryHelper.ReadCredentialValue(_siteName);
            if (valueFromRegistry == String.Empty)
            {
                return null;
            }

            var splitted = valueFromRegistry.Split(' ');
            byte[] login_ = Convert.FromBase64String(splitted[0]);
            byte[] pass_ = Convert.FromBase64String(splitted[1]);
            string login = Encoding.UTF8.GetString(ProtectedData.Unprotect(login_, Encoding.UTF8.GetBytes(_siteName), DataProtectionScope.CurrentUser));
            byte[] pass = ProtectedData.Unprotect(pass_, Encoding.UTF8.GetBytes(_siteName), DataProtectionScope.CurrentUser);
            SecureString ss = new SecureString();
            for (int i = 0; i < pass.Length; i++)
            {
                ss.AppendChar((char)pass[i]);
            }
            DataProtection.ClearByteArray(ref pass);

            return new Tuple<string, SecureString>(login, ss);
        }
    }
}
