﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace BooruGrabber.Util
{
    //используем синглтон, потому что со статическими классом/переменными были проблемы при биндинге
    public sealed class DownloadingSettings: INotifyPropertyChanged
    {
        private bool _saveTagsInJpegMetadata;
        public bool saveTagsInJpegMetadata
        {
            get
            {
                return _saveTagsInJpegMetadata;
            }
            set
            {
                _saveTagsInJpegMetadata = value;
                OnPropertyChanged("saveTagsInJpegMetadata");
                RegistryHelper.WriteSettingsValue(GetAllSettings());
            }
        }

        private bool _saveTagsInFileNames;
        public bool saveTagsInFileNames
        {
            get
            {
                if (allowSaveTagsInFileName)
                {
                    return _saveTagsInFileNames;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                _saveTagsInFileNames = value;
                OnPropertyChanged("saveTagsInFileNames");
                RegistryHelper.WriteSettingsValue(GetAllSettings());
            }
        }

        private bool _allowSaveTagsInFileName = true;
        public bool allowSaveTagsInFileName
        {
            get
            {
                return _allowSaveTagsInFileName;
            }
            set
            {
                _allowSaveTagsInFileName = value;
                OnPropertyChanged("allowSaveTagsInFileName");
            }
        }

        private bool _dontDownloadDoubles;
        public bool dontDownloadDoubles
        {
            get
            {
                return _dontDownloadDoubles;
            }
            set
            {
                _dontDownloadDoubles = value;
                OnPropertyChanged("dontDownloadDoubles");
                RegistryHelper.WriteSettingsValue(GetAllSettings());
            }
        }

        private bool _useAnonymizer;
        public bool useAnonymizer
        {
            get
            {
                return _useAnonymizer;
            }
            set
            {
                _useAnonymizer = value;
                OnPropertyChanged("useAnonymizer");
                RegistryHelper.WriteSettingsValue(GetAllSettings());
            }
        }

        private byte _threadsPackedCount;
        public byte threadsPackedCount
        {
            get
            {
                return _threadsPackedCount;
            }
            set
            {
                _threadsPackedCount = value;
                OnPropertyChanged("threadsPackedCount");
                RegistryHelper.WriteSettingsValue(GetAllSettings());
            }
        }
        public int threadsCount
        {
            get
            {
                return 1 << _threadsPackedCount;
            }
        }

        private ListSavingMethods _currentListSavingMethod;
        public ListSavingMethods currentListSavingMethod
        {
            get
            {
                return _currentListSavingMethod;
            }
            set
            {
                _currentListSavingMethod = value;
                RegistryHelper.WriteSettingsValue(GetAllSettings());
            }
        }
        public bool isCurrentListSavingMethodJSON
        {
            get
            {
                return (currentListSavingMethod == ListSavingMethods.JSON);
            }
            set
            {
                currentListSavingMethod = ListSavingMethods.JSON;
                //не особо нравится этот момент, похожий на какую-то костыльную тягу. Но не нашёл, как решить это красивее
                OnPropertyChanged("isCurrentListSavingMethodJSON");
                OnPropertyChanged("isCurrentListSavingMethodSQLite");
            }
        }
        public bool isCurrentListSavingMethodSQLite
        {
            get
            {
                return (currentListSavingMethod == ListSavingMethods.SQLite);
            }
            set
            {
                currentListSavingMethod = ListSavingMethods.SQLite;
                OnPropertyChanged("isCurrentListSavingMethodJSON");
                OnPropertyChanged("isCurrentListSavingMethodSQLite");
            }
        }


        public void Reset()
        {
            //saveTagsInJpegMetadata = false;
            //saveTagsInFileNames = true;
            allowSaveTagsInFileName = true;
            //dontDownloadDoubles = true;
            //hashFolder = false;
        }

        private static Object settLock = new object();

        private static DownloadingSettings _settings;
        public static DownloadingSettings settings
        {
            get
            {
                if (_settings == null)
                {
                    lock (settLock)
                    {
                        if (_settings == null)
                        {
                            _settings = new DownloadingSettings();
                            _settings.SetAllSettings(RegistryHelper.ReadSettingsValue());
                        }
                    }
                }

                return _settings;
            }
        }

        private DownloadingSettings()
        {
            
        }

        static DownloadingSettings()
        {

        }

        private void SetAllSettings(int localSettings)
        {
            _saveTagsInJpegMetadata = Convert.ToBoolean((localSettings & 1) >> 0);
            _saveTagsInFileNames = Convert.ToBoolean((localSettings & 2) >> 1);
            _dontDownloadDoubles = Convert.ToBoolean((localSettings & 4) >> 2);
            _useAnonymizer = Convert.ToBoolean((localSettings & 8) >> 3);
            _threadsPackedCount = Convert.ToByte((localSettings & (16 + 32)) >> 4);
            _currentListSavingMethod = (ListSavingMethods)Convert.ToInt32((localSettings & (64 + 128)) >> 6);
        }

        private int GetAllSettings()
        {
            int localSettings = 0;
            localSettings = localSettings | (Convert.ToInt32(saveTagsInJpegMetadata) << 0);
            localSettings = localSettings | (Convert.ToInt32(saveTagsInFileNames) << 1);
            localSettings = localSettings | (Convert.ToInt32(dontDownloadDoubles) << 2);
            localSettings = localSettings | (Convert.ToInt32(useAnonymizer) << 3);
            localSettings = localSettings | (Convert.ToInt32(threadsPackedCount) << 4);
            localSettings = localSettings | (Convert.ToInt32(currentListSavingMethod) << 6);

            return localSettings;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }

        public enum ListSavingMethods
        {
            JSON = 0,
            SQLite = 1
        }
    }
}
