﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BooruGrabber.Util.GrabbedData
{
    public class SimpleGrabbedFile: INotifyPropertyChanged
    {
        public string sourceSite { get; set; }
        public int id { get; set; }
        public string imagePath { get; set; }
        public string largerPreviewPath { get; set; }
        public string previewPath { get; set; }
        public string tags { get; set; }
        public string hash { get; set; }
        public string filename { get; set; }
        protected double _downloadingPercent;
        public double downloadingPercent
        {
            get
            {
                return _downloadingPercent;
            }
            set
            {
                SetDownloadingPercent(value);
            }
        }

        public bool downloadingState
        {
            get
            {
                return (downloadingPercent < 0);
            }
        }

        public void SetDownloadingPercent(double value)
        {
            _downloadingPercent = value;
            NotifyPropertyChanged("downloadingPercent");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
