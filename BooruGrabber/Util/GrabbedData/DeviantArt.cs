﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooruGrabber.Util.GrabbedData
{
    public class DeviantArt: AbstractGrabbedData
    {
        public string deviationid { get; set; }
        public object printid { get; set; }
        public string url { get; set; }
        public string title { get; set; }
        public string category { get; set; }
        public string category_path { get; set; }
        public bool? is_favourited { get; set; }
        public bool? is_deleted { get; set; }
        public DeviantArtAuthor author { get; set; }
        public DeviantArtStats stats { get; set; }
        public long? published_time { get; set; }
        public bool? allow_comments { get; set; }
        public DeviantArtFile preview { get; set; }
        public DeviantArtFile content { get; set; }
        public DeviantArtFile[] thumbs { get; set; }
        public bool? is_mature { get; set; }
        public bool? is_downloadable { get; set; }
        public long? download_filesize { get; set; }

        public string tags { get; set; }        //их мы заполняем отдельно, а не первым парсингом JSON

        public override bool Equals(AbstractGrabbedData other)
        {
            if (!(other is DeviantArt)) return false;
            return (other as DeviantArt).deviationid.Equals(this.deviationid);
        }

        public override int GetHashCode()
        {
            Guid guid;
            if (!Guid.TryParse(deviationid, out guid))
            {
                return deviationid.GetHashCode();
                //throw new Exception("DeviantArt на запрос вернул херню.");
            }
            return guid.GetHashCode();
        }

        public class DeviantArtAuthor
        {
            public string userid { get; set; }
            public string username { get; set; }
            public string usericon { get; set; }
            public string type { get; set; }
        }

        public class DeviantArtStats
        {
            public int? comments { get; set; }
            public int? favourites { get; set; }
        }

        public class DeviantArtFile
        {
            public string src { get; set; }
            public int? height { get; set; }
            public int? width { get; set; }
            public bool? transparency { get; set; }
            public int? filesize { get; set; }
        }

        public class DeviantArtMainResult
        {
            public bool? has_more { get; set; }
            public int? next_offset { get; set; }
            public DeviantArt[] results { get; set; }
        }

        public class DeviantArtTagsInfo
        {
            public DeviantArtMetadata[] metadata { get; set; }
        }

        public class DeviantArtMetadata
        {
            //на остальные пока пофиг
            public string deviationid { get; set; }
            public DeviantArtTag[] tags { get; set; }
        }

        public class DeviantArtTag
        {
            public string tag_name { get; set; }
            public bool? sponsored { get; set; }
            public string sponsor { get; set; }
        }
    }
}


/*
         "deviationid":"0D372F1F-0A6A-B40D-1DA4-21E654250EDB",
         "printid":null,
         "url":"http:\/\/miuranaoko.deviantart.com\/art\/Kiryuu-Moeka-569649170",
         "title":"Kiryuu Moeka",
         "category":"Movies & TV",
         "category_path":"fanart\/digital\/drawings\/movies",
         "is_favourited":false,
         "is_deleted":false,
         "author":{
            "userid":"4910AAEE-9D95-3492-E228-310B962E4445",
            "username":"MiuraNaoko",
            "usericon":"http:\/\/a.deviantart.net\/avatars\/m\/i\/miuranaoko.png?11",
            "type":"premium"
         },
         "stats":{
            "comments":14,
            "favourites":85
         },
         "published_time":1446400933,
         "allows_comments":true,
         "preview":{
            "src":"http:\/\/pre06.deviantart.net\/0fbb\/th\/pre\/f\/2015\/305\/e\/f\/kiryuu_moeka_by_naoko_00-d9f5k42.png",
            "height":966,
            "width":828,
            "transparency":true
         },
         "content":{
            "src":"http:\/\/orig04.deviantart.net\/bd50\/f\/2015\/305\/e\/f\/kiryuu_moeka_by_naoko_00-d9f5k42.png",
            "height":1400,
            "width":1200,
            "transparency":true,
            "filesize":1503592
         },
         "thumbs":[
            {
               "src":"http:\/\/t01.deviantart.net\/nW-Z4bers5taoSzbalZ4rd8KkIo=\/fit-in\/150x150\/filters:no_upscale():origin()\/pre06\/0fbb\/th\/pre\/f\/2015\/305\/e\/f\/kiryuu_moeka_by_naoko_00-d9f5k42.png",
               "height":150,
               "width":129,
               "transparency":true
            },
            {
               "src":"http:\/\/t11.deviantart.net\/ptZeV1t_Tc1SdqPytyUmmkBWOec=\/300x200\/filters:fixed_height(100,100):origin()\/pre06\/0fbb\/th\/pre\/f\/2015\/305\/e\/f\/kiryuu_moeka_by_naoko_00-d9f5k42.png",
               "height":200,
               "width":171,
               "transparency":true
            },
            {
               "src":"http:\/\/t05.deviantart.net\/PS8w_FEh_x7kByKpyKDoMO_jS30=\/fit-in\/300x900\/filters:no_upscale():origin()\/pre06\/0fbb\/th\/pre\/f\/2015\/305\/e\/f\/kiryuu_moeka_by_naoko_00-d9f5k42.png",
               "height":350,
               "width":300,
               "transparency":true
            }
         ],
         "is_mature":false,
         "is_downloadable":true,
         "download_filesize":1503592 
*/
