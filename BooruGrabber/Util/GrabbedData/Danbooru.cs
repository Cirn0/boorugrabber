﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooruGrabber.Util.GrabbedData
{
    class Danbooru: AbstractGrabbedData
    {
        //"id":2316567,"created_at":"2016-03-29T16:25:35.233-04:00","uploader_id":330014,"score":7,"source":"http://i2.pixiv.net/img-original/img/2016/03/14/15/06/31/55807673_p0.jpg","md5":"f106dff8c16592a8d7f524d4449093d6","last_comment_bumped_at":null,"rating":"s","image_width":1240,"image_height":1754,"tag_string":"1girl black_legwear blue_eyes bottle breasts brown_hair crossover dr_pepper highres kamkac long_hair looking_at_viewer makise_kurisu necktie off_shoulder pantyhose sitting skirt smile solo steins;gate","is_note_locked":false,"fav_count":12,"file_ext":"jpg","last_noted_at":null,"is_rating_locked":false,"parent_id":null,"has_children":false,"approver_id":null,"tag_count_general":17,"tag_count_artist":1,"tag_count_character":1,"tag_count_copyright":2,"file_size":570009,"is_status_locked":false,"fav_string":"fav:327639 fav:387186 fav:417959 fav:472413 fav:445481 fav:72291 fav:469777 fav:459949 fav:386036 fav:196099 fav:19831 fav:455446","pool_string":"","up_score":7,"down_score":0,"is_pending":false,"is_flagged":false,"is_deleted":false,"tag_count":21,"updated_at":"2016-03-29T16:25:35.233-04:00","is_banned":false,"pixiv_id":55807673,"last_commented_at":null,"has_active_children":false,"bit_flags":0,"uploader_name":"zaregoto","has_large":true,"tag_string_artist":"kamkac","tag_string_character":"makise_kurisu","tag_string_copyright":"dr_pepper steins;gate","tag_string_general":"1girl black_legwear blue_eyes bottle breasts brown_hair crossover highres long_hair looking_at_viewer necktie off_shoulder pantyhose sitting skirt smile solo","has_visible_children":false,"file_url":"/data/f106dff8c16592a8d7f524d4449093d6.jpg","large_file_url":"/data/sample/sample-f106dff8c16592a8d7f524d4449093d6.jpg","preview_file_url":"/data/preview/f106dff8c16592a8d7f524d4449093d6.jpg"

        public int id { get; set; }
        public DateTime? created_at { get; set; }
        public int? uploader_id { get; set; }
        public int? score { get; set; }
        public string source { get; set; }
        public string md5 { get; set; }
        public DateTime? last_comment_bumped_at { get; set; }
        public string rating { get; set; }
        public int? image_width { get; set; }
        public int? image_height { get; set; }
        public string tag_string { get; set; }
        public bool? is_note_locked { get; set; }
        public int? fav_count { get; set; }
        public string file_ext { get; set; }
        public DateTime? last_noted_at { get; set; }
        public bool? is_rating_locked { get; set; }
        public int? parent_id { get; set; }
        public bool? has_children { get; set; }
        public int? approver_id { get; set; }
        public int? tag_count_general { get; set; }
        public int? tag_count_artist { get; set; }
        public int? tag_count_character { get; set; }
        public int? tag_count_copyright { get; set; }
        public long? file_size { get; set; }
        public bool? is_status_locked { get; set; }
        public string fav_string { get; set; }
        public string pool_string { get; set; }
        public int? up_score { get; set; }
        public int? down_score { get; set; }
        public bool? is_pending { get; set; }
        public bool? is_flagged { get; set; }
        public bool? is_deleted { get; set; }
        public int? tag_sount { get; set; }
        public DateTime? updated_at { get; set; }
        public bool? is_banned { get; set; }
        public int? pixiv_id { get; set; }
        public DateTime? last_commented_at { get; set; }
        public bool? has_active_children { get; set; }
        public int? bit_flags { get; set; }
        public string uploader_name { get; set; }
        public bool? has_large { get; set; }
        public string tag_string_artist { get; set; }
        public string tag_string_character { get; set; }
        public string tag_string_copyright { get; set; }
        public string tag_string_general { get; set; }
        public bool? has_visible_children { get; set; }
        public string file_url { get; set; }
        public string large_file_url { get; set; }
        public string preview_file_url { get; set; }

        public override bool Equals(AbstractGrabbedData other)
        {
            if (!(other is Danbooru)) return false;
            return this.id == (other as Danbooru).id;
        }

        public override int GetHashCode()
        {
            return this.id;
        }
    }
}
