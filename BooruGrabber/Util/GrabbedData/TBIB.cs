﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooruGrabber.Util.GrabbedData
{
    public class TBIB : AbstractGrabbedData
    {
        //"directory":"4461","hash":"3c93a48faff8ed6d6c5f09a10733f9df","height":800,"id":4975669,"image":"0a39948319bc56ab0b7726c509ed20f703f43be9.jpg","change":1458532636,"owner":"danbooru","parent_id":0,"rating":"questionable","sample":false,"sample_height":0,"sample_width":0,"score":0,"tags":"1girl bare_arms bare_legs bare_shoulders bent_over blush bottomless bra breasts brown_eyes brown_hair cleavage collarbone copyright_request downblouse hair_over_shoulder hanging_breasts ino kousaka_rino leg_up long_hair looking_to_the_side otome_function panties panty_pull solo standing_on_one_leg thighs toes underwear underwear_only undressing white_bra white_panties wooden_floor","width":600
        public string directory { get; set; }
        public string hash { get; set; }
        public int height { get; set; }
        public int id { get; set; }
        public string image { get; set; }
        public long change { get; set; }
        public string owner { get; set; }
        public int parent_id { get; set; }
        public string rating { get; set; }
        public bool sample { get; set; }
        public int sample_height { get; set; }
        public int sample_width { get; set; }
        public int score { get; set; }
        public string tags { get; set; }
        public int width { get; set; }

        public override bool Equals(AbstractGrabbedData other)
        {
            if (!(other is TBIB)) return false;
            return this.id == (other as TBIB).id;
        }

        public override int GetHashCode()
        {
            return id;
        }
    }
}
