﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooruGrabber.Util.GrabbedData
{
    public class Safebooru: AbstractGrabbedData
    {
        //Sample:
        //"directory":"1641","hash":"2ce9ba5e76d69732452932c20b61ed61","height":1369,"id":1719300,"image":"2ce9ba5e76d69732452932c20b61ed61.jpeg","change":1458014510,"owner":"mioxnorman","parent_id":0,"rating":"safe","sample":true,"sample_height":776,"sample_width":850,"score":0,"tags":"blue_hair blush brown_eeys dress long_hair love_live!_school_idol_project smile sonoda_umi","width":1500
        //public string directory { get; set; }
        //public string hash { get; set; }
        //public int height { get; set; }
        //public int id { get; set; }
        //public string image { get; set; }
        //public long change { get; set; }
        //public string owner { get; set; }
        //public int parent_id { get; set; }
        //public string rating { get; set; }
        //public bool sample { get; set; }
        //public int sample_height { get; set; }
        //public int sample_width { get; set; }
        //public int score { get; set; }
        //public string tags { get; set; }
        //public int width { get; set; }
        public string directory { get; set; }
        public string hash { get; set; }
        public int height { get; set; }
        public int id { get; set; }
        public string image { get; set; }
        public long change { get; set; }
        public string owner { get; set; }
        public int parent_id { get; set; }
        public string rating { get; set; }
        public bool sample { get; set; }
        public int sample_height { get; set; }
        public int sample_width { get; set; }
        public int score { get; set; }
        public string tags { get; set; }
        public int width { get; set; }

        public override bool Equals(AbstractGrabbedData other)
        {
            if (!(other is Safebooru)) return false;
            return this.id == (other as Safebooru).id;
        }

        public override int GetHashCode()
        {
            return id;
        }
    }
}
