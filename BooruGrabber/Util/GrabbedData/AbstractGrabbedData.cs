﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooruGrabber.Util.GrabbedData
{
    public abstract class AbstractGrabbedData : IEquatable<AbstractGrabbedData>
    {
        public abstract bool Equals(AbstractGrabbedData other);

        public abstract override int GetHashCode();
    }
}
