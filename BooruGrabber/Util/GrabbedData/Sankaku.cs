﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooruGrabber.Util.GrabbedData
{
    public class Sankaku: AbstractGrabbedData
    {
        //[{"width":496,"has_notes":false,"status":"active","rating":"q","preview_width":105,"recommended_posts":0,"sample_url":"//cs.sankakucomplex.com/data/46/2e/462e468651951083c7a015efdc3f1fae.jpg?5204123","has_children":false,"preview_height":150,"preview_url":"//c.sankakucomplex.com/data/preview/46/2e/462e468651951083c7a015efdc3f1fae.jpg","change":18075842,"sample_width":496,"file_size":153571,"total_score":5,"source":"","author":"HentaFan","created_at":{"n":206222000,"json_class":"Time","s":1458876946},"sample_height":702,"file_url":"//cs.sankakucomplex.com/data/46/2e/462e468651951083c7a015efdc3f1fae.jpg?5204123","md5":"462e468651951083c7a015efdc3f1fae","height":702,"parent_id":null,"is_favorited":false,"vote_count":1,"tags":[{"type":0,"count":468058,"name_ja":"\u30c9\u30ec\u30b9","name":"dress","id":66},{"type":0,"count":1872826,"name_ja":"\u30ed\u30f3\u30b0\u30d8\u30a2","name":"long_hair","id":142},{"type":0,"count":322287,"name_ja":"\u7791\u76ee","name":"eyes_closed","id":4337},{"type":0,"count":1743079,"name_ja":"\u5973\u4e00\u4eba","name":"1girl","id":43623},{"type":0,"count":102348,"name_ja":"\u6c34","name":"water","id":599},{"type":0,"count":1212561,"name_ja":"\u4e73","name":"breasts","id":163},{"type":9,"count":44444,"name_ja":"\u7d75\u5e2b\u8a73\u7d30\u5e0c\u671b","name":"artist_request","id":7672},{"type":3,"count":16463,"name_ja":"\u30d5\u30a1\u30a4\u30a2\u30fc\u30a8\u30e0\u30d6\u30ec\u30e0","name":"fire_emblem","id":7479},{"type":0,"count":487314,"name_ja":"\u9752\u3044\u9aea","name":"blue_hair","id":53},{"type":3,"count":57604,"name_ja":"\u4efb\u5929\u5802","name":"nintendo","id":4537},{"type":0,"count":1133814,"name_ja":"\u5973\u6027","name":"female","id":34240},{"type":4,"count":369,"name_ja":"\u30a2\u30af\u30a2\uff08\u30d5\u30a1\u30a4\u30a2\u30fc\u30a8\u30e0\u30d6\u30ec\u30e0\uff09","name":"aqua_(fire_emblem)","id":794235},{"type":0,"count":1623,"name_ja":"\u30c0\u30f3\u30b5\u30fc","name":"dancer","id":10058}],"id":5204123,"has_comments":false,"fav_count":10}]
        public int width { get; set; }
        public bool has_notes { get; set; }
        public string status { get; set; }
        public string rating { get; set; }
        public int preview_width { get; set; }
        public object recommended_posts { get; set; }        //не совсем уверен, что там будет не массив, поэтому лучше сделаю тип object
        public string sample_url { get; set; }
        public bool has_children { get; set; }
        public int preview_height { get; set; }
        public string preview_url { get; set; }
        public long change { get; set; }
        public int sample_width { get; set; }
        public long file_size { get; set; }
        public int total_score { get; set; }
        public string source { get; set; }
        public string author { get; set; }
        public Sankaku_CreatedAt created_at { get; set; }
        public int sample_height { get; set; }
        public string file_url { get; set; }
        public string md5 { get; set; }
        public int height { get; set; }
        public object parent_id { get; set; }
        public bool is_favorited { get; set; }
        public int vote_count { get; set; }
        public Sankaku_Tags[] tags { get; set; }
        public int id { get; set; }
        public bool has_comments { get; set; }
        public int fav_count { get; set; }


        public class Sankaku_CreatedAt
        {
            public long n { get; set; }
            public string json_class { get; set; }
            public long s { get; set; }
        }

        public class Sankaku_Tags
        {
            //{"type":0,"count":1623,"name_ja":"\u30c0\u30f3\u30b5\u30fc","name":"dancer","id":10058}
            public int type { get; set; }
            public int count { get; set; }
            public string name_ja { get; set; }
            public string name { get; set; }
            public int id { get; set; }
        }

        public override bool Equals(AbstractGrabbedData other)
        {
            if (!(other is Sankaku)) return false;
            return this.id == (other as Sankaku).id;
        }

        public override int GetHashCode()
        {
            return id;
        }
    }
}
