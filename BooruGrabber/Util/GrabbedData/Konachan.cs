﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooruGrabber.Util.GrabbedData
{
    public class Konachan: AbstractGrabbedData
    {
        //actual_preview_height="190" actual_preview_width="300" author="MyCuteImouto" change="667742" created_at="1383927740" creator_id="106098" file_size="1411683" file_url="http://konachan.com/image/4a3ad359eae5d9c350176bbd3979ea77/Konachan.com%20-%20171823%20brown_hair%20kuri_kame%20long_hair%20makise_kurisu%20purple_eyes%20steins%3Bgate%20tie.jpg" frames="" frames_pending="" frames_pending_string="" frames_string="" has_children="false" height="760" id="171823" is_held="false" is_shown_in_index="true" jpeg_file_size="0" jpeg_height="760" jpeg_url="http://konachan.com/image/4a3ad359eae5d9c350176bbd3979ea77/Konachan.com%20-%20171823%20brown_hair%20kuri_kame%20long_hair%20makise_kurisu%20purple_eyes%20steins%3Bgate%20tie.jpg" jpeg_width="1200" md5="4a3ad359eae5d9c350176bbd3979ea77" preview_height="95" preview_url="http://konachan.com/data/preview/4a/3a/4a3ad359eae5d9c350176bbd3979ea77.jpg" preview_width="150" rating="s" sample_file_size="0" sample_height="760" sample_url="http://konachan.com/image/4a3ad359eae5d9c350176bbd3979ea77/Konachan.com%20-%20171823%20brown_hair%20kuri_kame%20long_hair%20makise_kurisu%20purple_eyes%20steins%3Bgate%20tie.jpg" sample_width="1200" score="155" source="http://i2.pixiv.net/img118/img/kuri_kame/37316170.jpg" status="active" tags="brown_hair kuri_kame long_hair makise_kurisu purple_eyes steins;gate tie" width="1200"
        public int? actual_preview_height { get; set; }
        public int? actual_preview_width { get; set; }
        public string author { get; set; }
        public int? change { get; set; }
        public long? created_at { get; set; }
        public int? creator_id { get; set; }
        public long? file_size { get; set; }
        public string file_url { get; set; }
        public object frames { get; set; }
        public object frames_pending { get; set; }
        public object frames_pending_string { get; set; }
        public object frames_string { get; set; }
        public bool? has_children { get; set; }
        public int? height { get; set; }
        public int id { get; set; }
        public bool? is_held { get; set; }
        public bool? is_shown_in_index { get; set; }
        public long? jpeg_file_size { get; set; }
        public int? jpeg_height { get; set; }
        public string jpeg_url { get; set; }
        public int? jpeg_width { get; set; }
        public string md5 { get; set; }
        public int? preview_height { get; set; }
        public string preview_url { get; set; }
        public int? preview_width { get; set; }
        public string rating { get; set; }
        public long? sample_file_size { get; set; }
        public int? sample_height { get; set; }
        public string sample_url { get; set; }
        public int? sample_width { get; set; }
        public int? score { get; set; }
        public string source { get; set; }
        public string status { get; set; }
        public string tags { get; set; }
        public int? width { get; set; }

        public override bool Equals(AbstractGrabbedData other)
        {
            if (!(other is Konachan)) return false;
            return this.id == (other as Konachan).id;
        }

        public override int GetHashCode()
        {
            return id;
        }
    }
}
