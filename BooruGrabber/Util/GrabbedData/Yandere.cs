﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooruGrabber.Util.GrabbedData
{
    public class Yandere : AbstractGrabbedData
    {
        //"id":349671,"tags":"huke makise_kurisu steins;gate","created_at":1458460641,"creator_id":10346,"author":"demonbane1349","change":1848704,"source":"","score":30,"md5":"f5346d20219ec75a8b0e1b1a21683021","file_size":4508730,"file_url":"https://files.yande.re/image/f5346d20219ec75a8b0e1b1a21683021/yande.re%20349671%20huke%20makise_kurisu%20steins%3Bgate.png","is_shown_in_index":true,"preview_url":"https://assets.yande.re/data/preview/f5/34/f5346d20219ec75a8b0e1b1a21683021.jpg","preview_width":107,"preview_height":150,"actual_preview_width":214,"actual_preview_height":300,"sample_url":"https://files.yande.re/sample/f5346d20219ec75a8b0e1b1a21683021/yande.re%20349671%20sample%20huke%20makise_kurisu%20steins%3Bgate.jpg","sample_width":1069,"sample_height":1500,"sample_file_size":403215,"jpeg_url":"https://files.yande.re/jpeg/f5346d20219ec75a8b0e1b1a21683021/yande.re%20349671%20huke%20makise_kurisu%20steins%3Bgate.jpg","jpeg_width":1782,"jpeg_height":2500,"jpeg_file_size":1128758,"rating":"s","has_children":false,"parent_id":null,"status":"active","width":1782,"height":2500,"is_held":false,"frames_pending_string":"","frames_pending":[],"frames_string":"","frames":[]
        public int? id { get; set; }
        public string tags { get; set; }
        public long? created_at { get; set; }
        public int? creator_id { get; set; }
        public string author { get; set; }
        public int? change { get; set; }
        public string source { get; set; }
        public int? score { get; set; }
        public string md5 { get; set; }
        public long? file_size { get; set; }
        public string file_url { get; set; }
        public bool? is_shown_in_index { get; set; }
        public string preview_url { get; set; }
        public int? preview_width { get; set; }
        public int? preview_height { get; set; }
        public int? actual_preview_width { get; set; }
        public int? actual_preview_height { get; set; }
        public string sample_url { get; set; }
        public int? sample_width { get; set; }
        public int? sample_height { get; set; }
        public long? sample_file_size { get; set; }
        public string jpeg_url { get; set; }
        public int? jpeg_width { get; set; }
        public int? jpeg_height { get; set; }
        public long? jpeg_file_size { get; set; }
        public string rating { get; set; }
        public bool? has_children { get; set; }
        public int? parent_id { get; set; }
        public string status { get; set; }
        public int? width { get; set; }
        public int? height { get; set; }
        public bool? is_held { get; set; }
        public object frames_pending_string { get; set; }
        public object frames_pending { get; set; }
        public object frames_string { get; set; }
        public object frames { get; set; }

        public override bool Equals(AbstractGrabbedData other)
        {
            if (!(other is Yandere)) return false;
            return this.id == (other as Yandere).id;
        }

        public override int GetHashCode()
        {
            return (int)id;
        }
    }
}
