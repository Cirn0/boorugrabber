﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooruGrabber.Util.GrabbedData
{
    public class TheDoujin : AbstractGrabbedData, IEquatable<AbstractGrabbedData>
    {
        //{"category_id":"13551","title":"[DRAGON PANDA] Idaten R2 (Touhou) [ENG]","translation_type":"","artist":"","tags":"cunnilingus english fellatio futanari hat inubashiri_momiji kawashiro_nitori large_breasts loli long_hair shameimaru_aya swimsuit tagme touhou touhou_project translated vaginal yuri","rating":"e","score":"0","last_comment":null,"uploaded_by":"7599","directory":"ca\/98","file_name":"ca9853491e50f1268ca30dd387eb0316.jpg","md5":"ca9853491e50f1268ca30dd387eb0316","file_url":"http:\/\/thedoujin.com\/images\/ca\/98\/ca9853491e50f1268ca30dd387eb0316.jpg","preview_url":"http:\/\/thedoujin.com\/thumbnails\/ca\/98\/thumbnail_ca9853491e50f1268ca30dd387eb0316.jpg","image_domain":"http:\/\/thedoujin.com\/images\/"
        public int category_id { get; set; }
        public string title { get; set; }
        public string translation_type { get; set; }
        public string artist { get; set; }
        public string tags { get; set; }
        public string rating { get; set; }
        public int? score { get; set; }
        public int? last_comment { get; set; }
        public int? uploaded_by { get; set; }
        public string directory { get; set; }
        public string file_name { get; set; }
        public string md5 { get; set; }
        public string file_url { get; set; }
        public string preview_url { get; set; }
        public string image_domain{ get; set; }
        public TheDoujinPage[] pages { get; set; }

        public class TheDoujinPage
        {
            //{"id":"838077","directory":"84\/b4","file_name":"84b4110ab2d457f10d43bdc48d409759.jpg","md5":"84b4110ab2d457f10d43bdc48d409759","sorted_order":"0","image_domain":"http:\/\/thedoujin.com\/images\/","file_url":"http:\/\/thedoujin.com\/images\/84\/b4\/84b4110ab2d457f10d43bdc48d409759.jpg","preview_url":"http:\/\/thedoujin.com\/thumbnails\/84\/b4\/thumbnail_84b4110ab2d457f10d43bdc48d409759.jpg"}
            public int id { get; set; }
            public string directory { get; set; }
            public string file_name { get; set; }
            public string md5 { get; set; }
            public int sorted_order { get; set; }
            public string image_domain { get; set; }
            public string file_url { get; set; }
            public string preview_url { get; set; }
        }

        public override bool Equals(AbstractGrabbedData other)
        {
            if (!(other is TheDoujin)) return false;
            return this.category_id == (other as TheDoujin).category_id;
        }

        public override int GetHashCode()
        {
            return this.category_id;
        }
    }
}
