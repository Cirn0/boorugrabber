﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooruGrabber.Util.GrabbedData
{
    class Zerochan : AbstractGrabbedData
    {
        public int id { get; set; }
        public string imagePath { get; set; }
        public string largerPreviewPath { get; set; }
        public string previewPath { get; set; }
        public string tags { get; set; }
        public string filename { get; set; }

        public override bool Equals(AbstractGrabbedData other)
        {
            if (!(other is Zerochan)) return false;
            return this.id == (other as Zerochan).id;
        }

        public override int GetHashCode()
        {
            return id;
        }
    }
}
