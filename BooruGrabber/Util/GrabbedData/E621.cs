﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooruGrabber.Util.GrabbedData
{
    class E621: AbstractGrabbedData
    {
        //"id":848873,"tags":"abstract_background anthro black_hair black_nose blush bottomless canine chest_tuft chest_wraps clothed clothing female flat_chested fox fur furrification hair hair_ribbon hi_res japanese_text kemono long_hair mammal multi_nipple nipples open_mouth red_eyes reimu_hakurei ribbons slit_pupils solo standing text touhou tuft white_fur wraps yukaran_nosuke","description":"","created_at":{"json_class":"Time","s":1457927173,"n":943148000},"creator_id":134487,"author":"donteven","change":7489055,"source":"https://twitter.com/yukaran_nosuke","score":12,"fav_count":56,"md5":"b056d32e1a216d29aaa62ac9466340e8","file_size":383587,"file_url":"https://static1.e621.net/data/b0/56/b056d32e1a216d29aaa62ac9466340e8.jpg","file_ext":"jpg","preview_url":"https://static1.e621.net/data/preview/b0/56/b056d32e1a216d29aaa62ac9466340e8.jpg","preview_width":101,"preview_height":150,"sample_url":"https://static1.e621.net/data/sample/b0/56/b056d32e1a216d29aaa62ac9466340e8.jpg","sample_width":540,"sample_height":800,"rating":"q","has_children":false,"children":"845631","parent_id":null,"status":"active","width":1378,"height":2039,"has_comments":false,"has_notes":false,"artist":["yukaran_nosuke"],"sources":["https://twitter.com/yukaran_nosuke","https://twitter.com/yukaran_nosuke/status/706861235627696128","https://pbs.twimg.com/media/Cc9GjCNVAAAGuC_.jpg:orig"]

        //не буду особо заморачиваться и делать обёртку для сложных структур - пускай сидят как object, всё равно они мне по сути пока не нужны
        public int id { get; set; }
        public string tags { get; set; }
        public string description { get; set; }
        public object created_at { get; set; }
        public int? creator_id { get; set; }
        public string author { get; set; }
        public long? change { get; set; }
        public string source { get; set; }
        public int? score { get; set; }
        public int? fav_count { get; set; }
        public string md5 { get; set; }
        public int? file_size { get; set; }
        public string file_url { get; set; }
        public string file_ext { get; set; }
        public string preview_url { get; set; }
        public int? preview_width { get; set; }
        public int? preview_height { get; set; }
        public string sample_url { get; set; }
        public int? sample_width { get; set; }
        public int? sample_height { get; set; }
        public string rating { get; set; }
        public bool? has_children { get; set; }
        public object children { get; set; }
        public int? parent_id { get; set; }
        public string status { get; set; }
        public int? width { get; set; }
        public int? height { get; set; }
        public bool? has_comments { get; set; }
        public bool? has_notes { get; set; }
        public object artist { get; set; }
        public object sources { get; set; }

        public override bool Equals(AbstractGrabbedData other)
        {
            if (!(other is E621)) return false;
            return this.id == (other as E621).id;
        }

        public override int GetHashCode()
        {
            return (int)id;
        }
    }
}
