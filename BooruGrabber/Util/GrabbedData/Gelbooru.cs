﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooruGrabber.Util.GrabbedData
{
    class Gelbooru: AbstractGrabbedData
    {
        //"directory":"f1\/06","hash":"f106dff8c16592a8d7f524d4449093d6","height":1754,"id":3113944,"image":"f106dff8c16592a8d7f524d4449093d6.jpg","change":1459283509,"owner":"danbooru","parent_id":null,"rating":"s","sample":true,"sample_height":1202,"sample_width":850,"score":1,"tags":"1girl black_legwear blue_eyes bottle breasts brown_hair crossover dr_pepper highres kamkac long_hair looking_at_viewer makise_kurisu necktie off_shoulder pantyhose sitting skirt smile solo steins;gate","width":1240,"file_url":"http:\/\/simg4.gelbooru.com\/images\/f1\/06\/f106dff8c16592a8d7f524d4449093d6.jpg"}

        public string directory { get; set; }
        public string hash { get; set; }
        public int height { get; set; }
        public int id { get; set; }
        public string image { get; set; }
        public long change { get; set; }
        public string owner { get; set; }
        public int? parent_id { get; set; }
        public string rating { get; set; }
        public bool? sample { get; set; }
        public int? sample_height { get; set; }
        public int? sample_width { get; set; }
        public int? score { get; set; }
        public string tags { get; set; }
        public int width { get; set; }
        public string file_url { get; set; }
        
        public override bool Equals(AbstractGrabbedData other)
        {
            if (!(other is Gelbooru)) return false;
            return this.id == (other as Gelbooru).id;
        }

        public override int GetHashCode()
        {
            return this.id;
        }
    }
}
