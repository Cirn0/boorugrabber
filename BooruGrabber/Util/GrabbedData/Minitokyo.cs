﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooruGrabber.Util.GrabbedData
{
    class Minitokyo: AbstractGrabbedData
    {
        public int id { get; set; }
        public string imagePath { get; set; }
        public string largerPreviewPath { get; set; }
        public string previewPath { get; set; }
        public string tags { get; set; }
        public string filename { get; set; }

        public override bool Equals(AbstractGrabbedData other)
        {
            if (!(other is Minitokyo)) return false;
            return this.id == (other as Minitokyo).id;
        }

        public override int GetHashCode()
        {
            return id;
        }
    }
}
