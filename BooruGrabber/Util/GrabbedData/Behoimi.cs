﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooruGrabber.Util.GrabbedData
{
    class Behoimi: AbstractGrabbedData
    {
        //"status":"active","creator_id":1,"preview_width":99,"source":"http://pds.exblog.jp/pds/1/201603/21/41/f0130741_013294.jpg","author":"nil!","width":800,"score":0,"preview_height":150,"has_comments":false,"sample_width":800,"has_children":false,"sample_url":"http://behoimi.org/data/0f/30/0f30af49f9e413a1e905f97959bf112a.jpg","file_url":"http://behoimi.org/data/0f/30/0f30af49f9e413a1e905f97959bf112a.jpg","parent_id":null,"sample_height":1202,"md5":"0f30af49f9e413a1e905f97959bf112a","tags":"cosplay glasses megami_tensei nagisa_mark-02 persona persona_4 pleated_skirt satonaka_chie skirt track_jacket","change":1191590,"has_notes":false,"rating":"s","id":621446,"height":1202,"preview_url":"http://behoimi.org/data/preview/0f/30/0f30af49f9e413a1e905f97959bf112a.jpg","file_size":306729,"created_at":{"json_class":"Time","n":995000000,"s":1459351164}

        public string status { get; set; }
        public int creator_id { get; set; }
        public int preview_width { get; set; }
        public string source { get; set; }
        public string author { get; set; }
        public int width { get; set; }
        public int score { get; set; }
        public int preview_height { get; set; }
        public bool has_comments { get; set; }
        public int sample_width { get; set; }
        public bool has_children { get; set; }
        public string sample_url { get; set; }
        public string file_url { get; set; }
        public int? parent_id { get; set; }
        public int sample_height { get; set; }
        public string md5 { get; set; }
        public string tags { get; set; }
        public long change { get; set; }
        public bool has_notes { get; set; }
        public string rating { get; set; }
        public int id { get; set; }
        public int height { get; set; }
        public string preview_url { get; set; }
        public long file_size { get; set; }
        public Behoimi_CreatedAt created_at { get; set; }

        public class Behoimi_CreatedAt
        {
            public string json_class { get; set; }
            public long n { get; set; }
            public long s { get; set; }
        }

        public override bool Equals(AbstractGrabbedData other)
        {
            if (!(other is Behoimi)) return false;
            return this.id == (other as Behoimi).id;
        }

        public override int GetHashCode()
        {
            return id;
        }
    }
}
