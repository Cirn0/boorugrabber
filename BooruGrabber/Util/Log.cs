﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooruGrabber.Util
{
    public class Log: INotifyPropertyChanged
    {
        private List<LogEntry> _messages { get; set; } = new List<LogEntry>();

        private string tempMessages = String.Empty;
        public string messages
        {
            get
            {
                return tempMessages;
            }
        }

        public void AddEntry(MessageType type, string message)
        {
            var v = new LogEntry(type, message);
            _messages.Add(v);
            tempMessages += ((tempMessages != String.Empty) ? Environment.NewLine : String.Empty) + v.ToString();
            OnPropertyChanged("messages");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }

        private static Object settLock = new object();

        private static Log _log;
        public static Log log
        {
            get
            {
                if (_log == null)
                {
                    lock (settLock)
                    {
                        if (_log == null)
                        {
                            _log = new Log();
                        }
                    }
                }

                return _log;
            }
        }

        private Log()
        {

        }

        static Log()
        {

        }

        public class LogEntry
        {
            private MessageType _messType;
            public MessageType messageType
            {
                get
                {
                    return _messType;
                }
                set
                {
                    _messType = value;
                }
            }
            private string _mess;
            private DateTime _time;

            public LogEntry(MessageType type, string message)
            {
                _messType = type;
                _mess = message;
                _time = DateTime.Now;
            }

            public override string ToString()
            {
                return _time.ToString("HH:mm:ss") + " " + _mess;
            }
        }

        public enum MessageType
        {
            notice,
            warning,
            critical
        }
    }
}
