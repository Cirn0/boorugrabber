﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Security.Cryptography;
using Microsoft.Win32;
using System.ComponentModel;
using System.Globalization;

namespace BooruGrabber.Util
{
    /// <summary>
    /// Interaction logic for SettingsForm.xaml
    /// </summary>
    public partial class SettingsForm : Window
    {
        public SettingsForm()
        {
            InitializeComponent();
        }
        
        //DPAPI - максимум, на что я могу рассчитывать без принудительного ввода пользователем мастер-ключа (кому это нахрен надо для пары сайтов с картинками?)
        private void button_Click(object sender, RoutedEventArgs e)
        {
            var v = LoginInfoInput.GetLoginAndPassword(null, (string)((ComboBoxItem)siteNameComboBox.SelectedItem).Tag);
            if (v != null)
            {
                string siteTag = (string)((ComboBoxItem)siteNameComboBox.SelectedItem).Tag;
                DataProtection.StoreCredentialsInRegistry(siteTag, v.Item1, v.Item2);
            }
        }

        private void siteNameComboBox_DropDownOpened(object sender, EventArgs e)
        {
            RegistryHelper.EnumerateCredentialValues().ForEach(a => 
            {
                ((ComboBoxItem)this.FindName(a)).Background = Brushes.LightGreen;
            });
        }

        private void siteNameComboBox_DropDownClosed(object sender, EventArgs e)
        {
            foreach (var v in siteNameComboBox.Items)
            {
                if (v is ComboBoxItem)
                {
                    ((ComboBoxItem)v).Background = Brushes.Transparent;
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            RegistryHelper.WriteWindowState("settings", new RegistryHelper.WindowState() { height = this.Height, left = this.Left, top = this.Top, width = this.Width });
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            var v = RegistryHelper.ReadWindowStateValue("settings");
            if (v == null)
            {
                return;
            }
            this.Height = v.height;
            this.Width = v.width;
            this.Left = v.left;
            this.Top = v.top;
        }
    }

    public class SliderValueToThreadsCountConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            int val = System.Convert.ToInt32(value);
            return 1 << (val);
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            int val = System.Convert.ToInt32(value);
            return (int) Math.Round(Math.Sqrt(val));
        }
    }
}
