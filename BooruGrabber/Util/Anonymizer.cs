﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BooruGrabber.Util.GrabbedData;

namespace BooruGrabber.Util
{
    //История использованных анонимайзеров:
    //1. whoer.net  — отказался, так как он ещё и заменяет ссылки в JSON-данных, а эти ссылки слишком быстро истекают.
    //2. kalarupa.com   — отказался, так как он почему-то некорректно отрабатывал для полных картинок с яндере и косячил с TheDoujin.com
    //3. proxy-unlock.com   — пока что работает
    public static class Anonymizer
    {
        public static byte[] LoadDataThrough(string url_, SimpleGrabbedFile file = null)
        {
            byte[] result = null;
            HttpWebRequest hwr = null;
            HttpWebResponse response = null;
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    hwr = WebRequest.CreateHttp("http://proxy-unlock.com/index.php");
                    hwr.Proxy = WebRequest.DefaultWebProxy;
                    hwr.UserAgent = "Other";
                    hwr.Method = WebRequestMethods.Http.Post;
                    hwr.AllowAutoRedirect = true;
                    hwr.Referer = "http://proxy-unlock.com/index.php";
                    hwr.Host = "proxy-unlock.com";
                    hwr.ContentType = "application/x-www-form-urlencoded";
                    hwr.CookieContainer = new CookieContainer(1);
                    hwr.CookieContainer.Add(new Cookie("flags","2ed","/",".proxy-unlock.com"));

                    string url = TagCorrector.CorrectLink(url_);
                    byte[] array = new byte["q=&hl[include_form]=on&hl[accept_cookies]=on&hl[show_images]=on&hl[show_referer]=on&hl[base64_encode]=on&hl[strip_meta]=on&hl[session_cookies]=on".Length + url.Length];
                    Encoding.UTF8.GetBytes(String.Format("q={0}&hl[include_form]=on&hl[accept_cookies]=on&hl[show_images]=on&hl[show_referer]=on&hl[base64_encode]=on&hl[strip_meta]=on&hl[session_cookies]=on", url)).CopyTo(array, 0);
                    hwr.ContentLength = array.Length;

                    using (var stream = hwr.GetRequestStream())
                    {
                        stream.Write(array, 0, array.Length);
                        stream.Flush();
                    }

                    string link = String.Empty;
                    using (response = (HttpWebResponse)hwr.GetResponse())
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            if (file != null)
                            {
                                result = GetContentWithProgressReporting(stream, (response.ContentLength != -1) ? response.ContentLength : 0, file);
                            }
                            else
                            {
                                using (MemoryStream sr = new MemoryStream())
                                {
                                    stream?.CopyTo(sr);
                                    result = sr.ToArray();
                                }
                            }
                        }
                    }
                    break;
                }
                catch (Exception)
                {
                    //repeat
                }
                finally
                {
                    if (result == null)
                    {
                        Log.log.AddEntry(Log.MessageType.warning, String.Format("Попытка {0} из 5. Запрос завершён с ошибкой.", i + 1));
                    }
                    response?.Close();
                    response?.Dispose();
                    hwr?.Abort();
                }
            }

            return result;
        }

        private static byte[] GetContentWithProgressReporting(Stream responseStream, long contentLength, SimpleGrabbedFile file)
        {
            file.SetDownloadingPercent(0);
            byte[] data;
            if (contentLength > 0)
            {
                data = new byte[contentLength];
                int currentIndex = 0;
                int buffSize = (int)(contentLength / 20);
                var buffer = new byte[buffSize];
                do
                {
                    //var bytesReceived = responseStream.Read(data, currentIndex, buffSize);
                    var bytesReceived = responseStream.Read(buffer, 0, buffSize);
                    Array.Copy(buffer, 0, data, currentIndex, bytesReceived);
                    currentIndex += bytesReceived;

                    // Report percentage
                    double percentage = (double)currentIndex / contentLength;
                    file.SetDownloadingPercent(percentage * 100);
                } while (currentIndex < contentLength);
            }
            else
            {
                //в таком случае сервер нам не передал contentLength. Выкручиваемся как можем
                file.SetDownloadingPercent(-1);
                using (MemoryStream sr = new MemoryStream())
                {
                    responseStream.CopyTo(sr);
                    sr.Position = 0;
                    data = new byte[sr.Length];
                    sr.Read(data, 0, data.Length);
                }
            }

            file.SetDownloadingPercent(100);

            return data;
        }
    }
}