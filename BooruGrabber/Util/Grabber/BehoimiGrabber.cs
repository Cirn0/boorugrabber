﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooruGrabber.Util.GrabbedData;
using System.Net;
using System.Web.Helpers;
using System.Security.Cryptography;
using System.Windows;
using System.Security;

namespace BooruGrabber.Util.Grabber
{
    class BehoimiGrabber: AbstractJsonGrabber
    {
        public BehoimiGrabber()
        {
            hashAlgorithm = new MD5CryptoServiceProvider();
        }

        public override SimpleGrabbedFile DoConvertion(AbstractGrabbedData data)
        {
            Behoimi current = (Behoimi)data;
            SimpleGrabbedFile @return = new SimpleGrabbedFile();
            @return.id = current.id;
            @return.hash = current.md5;
            @return.imagePath = current.file_url;
            @return.previewPath = current.preview_url;
            @return.largerPreviewPath = current.sample_url;
            @return.tags = current.tags;
            @return.filename = String.Concat(current.file_url.Reverse().TakeWhile(a => !a.Equals('/')).Reverse());
            @return.sourceSite = "Behoimi";
            return @return;
        }

        protected override string GetURL(string tag, int pageNumber)
        {
            return "http://behoimi.org/post/index.json?limit=100&page=" + (pageNumber + 1).ToString() + "&tags=" + tag;
        }

        protected override List<AbstractGrabbedData> ProcessJsonData(byte[] json)
        {
            string str = Encoding.UTF8.GetString(json);
            dynamic d = Json.Decode(str, typeof(Behoimi[]));
            if (d == null || d.Length == 0) return null;
            object[] temp = (object[])d;

            return temp.Select(a => (AbstractGrabbedData)((Behoimi)a)).ToList();
        }

        protected override string GetReferer()
        {
            return "http://behoimi.org/post/show";
        }
    }
}
