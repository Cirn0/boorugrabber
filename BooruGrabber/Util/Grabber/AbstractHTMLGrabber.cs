﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooruGrabber.Util.GrabbedData;

namespace BooruGrabber.Util.Grabber
{
    public abstract class AbstractHTMLGrabber : AbstractGrabber
    {
        protected override bool CompareHash(byte[] filesData, string hash)
        {
            return true;        //интересно, какие хэши мы сможем спарсить из HTML-страницы
        }
    }
}
