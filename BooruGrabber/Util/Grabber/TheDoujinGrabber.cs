﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooruGrabber.Util.GrabbedData;
using System.Security.Cryptography;
using System.Web.Helpers;
using System.Net;
using System.IO;

namespace BooruGrabber.Util.Grabber
{
    class TheDoujinGrabber : AbstractJsonGrabber
    {
        public TheDoujinGrabber()
        {
            hashAlgorithm = new MD5CryptoServiceProvider();
        }

        public override SimpleGrabbedFile DoConvertion(AbstractGrabbedData data)
        {
            TheDoujin current = (TheDoujin)data;
            SimpleGrabbedFile @return = new SimpleGrabbedFile();
            @return.id = current.category_id;
            @return.hash = current.md5;
            @return.imagePath = current.file_url;
            @return.previewPath = current.preview_url;
            @return.largerPreviewPath = current.preview_url;
            @return.tags = current.tags;
            @return.filename = current.title;
            @return.sourceSite = "TheDoujin";
            return @return;
        }

        protected override string GetURL(string tag, int pageNumber)
        {
            //отсчёт начинается не с нулевой, а с первой страницы, поэтому +1
            return "https://thedoujin.com/index.php/api/categories?json=1&Categories_page=" + (pageNumber + 1).ToString() + "&tags=" + tag;
        }

        protected override List<AbstractGrabbedData> ProcessJsonData(byte[] json)
        {
            string str = Encoding.UTF8.GetString(json);
            dynamic d = Json.Decode(str, typeof(TheDoujin[]));
            if (d == null || d.Length == 0) return null;
            object[] temp = (object[])d;

            return temp.Select(a => (AbstractGrabbedData)((TheDoujin)a)).ToList();
        }

        protected List<TheDoujin.TheDoujinPage> ProcessJsonPagesData(string json)
        {
            dynamic d = Json.Decode(json, typeof(TheDoujin.TheDoujinPage[]));
            if (d == null || d.Length == 0) return null;
            object[] temp = (object[])d;

            return temp.Select(a => (TheDoujin.TheDoujinPage)a).ToList();
        }

        protected override void ImagesDownloadingTask(object _folderPath)
        {
            while (9 == 9)
            {
                if (noMore)
                {
                    return;
                }
                string folderPath = (string)_folderPath;
                SimpleGrabbedFile currentFile = null;
                lock (filesIterationIndexLock)
                {
                    if (filesToDownload.Count <= filesIterationIndex)
                    {
                        noMore = true;
                        return;
                    }
                    currentFile = filesToDownload[filesIterationIndex];
                    filesIterationIndex++;
                }

                _currentlyProcessedFile = currentFile;
                string url = GetGalleryFilesListUrl(currentFile.id);
                HttpWebRequest hwr = WebRequest.CreateHttp(url);
                hwr.Proxy = WebRequest.DefaultWebProxy;
                hwr.UserAgent = "Other";
                hwr.Method = WebRequestMethods.Http.Get;
                hwr.Accept = "application/json";
                string text = String.Empty;
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        var response = hwr.GetResponse();
                        using (var sr = new StreamReader(response.GetResponseStream()))
                        {
                            text = sr.ReadToEnd();
                        }
                        break;
                    }
                    catch
                    {
                        //repeat
                    }
                }
                if (text == String.Empty)
                {
                    continue;
                }
                var ret = ProcessJsonPagesData(text);

                Directory.CreateDirectory(folderPath + "\\" + currentFile.filename);

                ret.ForEach(a =>
                {
                    byte[] b = null;
                    for (int i = 0; i < 5; i++)
                    {
                        try
                        {
                            var file = new SimpleGrabbedFile() { imagePath = a.file_url };      //поскольку при скачивании файла нас особо не волнует ничего, кроме самой ссылки на файл, прикручиваем вот такой костыль
                            b = DownloadFile_(file);
                            if (CompareHash(b, a.md5))
                                break;
                            else
                                b = null;
                        }
                        catch (WebException)
                        {
                        //repeat
                    }
                    }
                    if (b == null)
                    {
                        return;
                    }

                    if ((folderPath + "\\" + currentFile.filename + "\\" + a.file_name).Length > 260 - ret.Count().ToString().Length - 1)        //мы в заднице. надо исправлять
                    {
                        //TODO: сделать с этим что-нибудь
                        throw new NotImplementedException();
                    }

                    string fileName = folderPath + "\\" + currentFile.filename + "\\" + a.sorted_order.ToString().PadLeft(ret.Count.ToString().Length, '0') + "_" + a.file_name;

                    System.IO.File.WriteAllBytes(fileName, b);      //если не jpeg - тэги не сохраняем

                currentFile.downloadingPercent += (1.0 / ret.Count()) * 100;
                });

                _downloadedCount += 1;
            }
        }

        protected string GetGalleryFilesListUrl(int galleryID)
        {
            return "http://thedoujin.com/index.php/api/pages/" + galleryID.ToString() + "?json=1";
        }
    }
}
