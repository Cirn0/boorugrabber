﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooruGrabber.Util.GrabbedData;
using System.Net;
using System.Web.Helpers;
using System.Security.Cryptography;
using System.Windows;

namespace BooruGrabber.Util.Grabber
{
    class GelbooruGrabber: AbstractJsonGrabber
    {
        public GelbooruGrabber()
        {
            hashAlgorithm = new MD5CryptoServiceProvider();
        }

        public override SimpleGrabbedFile DoConvertion(AbstractGrabbedData data)
        {
            try
            {
                Gelbooru current = (Gelbooru)data;
                SimpleGrabbedFile @return = new SimpleGrabbedFile();
                @return.id = current.id;
                @return.hash = current.hash;
                @return.imagePath = current.file_url;
                @return.previewPath = current.file_url;         //почему-то не нашёл у него в данных ссылку на превьюхи, 
                @return.largerPreviewPath = current.file_url;   //так что пусть качает целиком, чего уж поделать
                @return.tags = current.tags;
                @return.filename = current.image;
                @return.sourceSite = "Gelbooru";
                return @return;
            }
            catch
            {
                return null;
            }
        }

        protected override string GetURL(string tag, int pageNumber)
        {
            return "http://gelbooru.com/index.php?page=dapi&s=post&q=index&limit=100&pid=" + (pageNumber).ToString() + "&tags=" + tag + "&json=1";
        }

        protected override List<AbstractGrabbedData> ProcessJsonData(byte[] json)
        {
            string str = Encoding.UTF8.GetString(json);
            dynamic d = Json.Decode(str, typeof(Gelbooru[]));
            if (d == null || d.Length == 0) return null;
            object[] temp = (object[])d;

            return temp.Select(a => (AbstractGrabbedData)((Gelbooru)a)).ToList();
        }
    }
}
