﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooruGrabber.Util.GrabbedData;
using System.Net;
using System.Web.Helpers;
using System.Security.Cryptography;
using System.Windows;

namespace BooruGrabber.Util.Grabber
{
    class DanbooruGrabber : AbstractJsonGrabber
    {
        public DanbooruGrabber()
        {
            hashAlgorithm = new MD5CryptoServiceProvider();
        }

        public override SimpleGrabbedFile DoConvertion(AbstractGrabbedData data)
        {
            try
            {
                Danbooru current = (Danbooru)data;
                SimpleGrabbedFile @return = new SimpleGrabbedFile();
                @return.id = current.id;
                @return.hash = current.md5;
                @return.imagePath = "http://danbooru.donmai.us" + current.file_url;
                @return.previewPath = "http://danbooru.donmai.us" + current.preview_file_url;
                @return.largerPreviewPath = "http://danbooru.donmai.us" + ((current.has_large ?? false) ? current.large_file_url : current.preview_file_url);
                @return.tags = current.tag_string;
                @return.filename = String.Concat(current.file_url.Reverse().TakeWhile(a => !a.Equals('/')).Reverse());
                @return.sourceSite = "Danbooru";
                return @return;
            }
            catch
            {
                return null;
            }
        }

        protected override string GetURL(string tag, int pageNumber)
        {
            return "http://danbooru.donmai.us/posts.json?limit=100&page=" + (pageNumber + 1).ToString() + "&tags=" + tag;
        }

        protected override List<AbstractGrabbedData> ProcessJsonData(byte[] json)
        {
            string str = Encoding.UTF8.GetString(json);
            dynamic d = Json.Decode(str, typeof(Danbooru[]));
            if (d == null || d.Length == 0) return null;
            object[] temp = (object[])d;

            return temp.Select(a => ((((Danbooru)a).is_banned ?? false) || (((Danbooru)a).is_deleted ?? false)) ? null : (AbstractGrabbedData)((Danbooru)a)).ToList();
        }
    }
}
