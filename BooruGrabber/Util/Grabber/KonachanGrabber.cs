﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooruGrabber.Util.GrabbedData;
using System.Net;
using System.Web.Helpers;
using System.Security.Cryptography;
using System.Windows;

namespace BooruGrabber.Util.Grabber
{
    class KonachanGrabber : AbstractJsonGrabber
    {
        public KonachanGrabber()
        {
            hashAlgorithm = new MD5CryptoServiceProvider();
        }

        public override SimpleGrabbedFile DoConvertion(AbstractGrabbedData data)
        {
            Konachan current = (Konachan)data;
            SimpleGrabbedFile @return = new SimpleGrabbedFile();
            @return.id = current.id;
            @return.hash = current.md5;
            @return.imagePath = current.file_url;
            @return.previewPath = current.preview_url;
            @return.largerPreviewPath = current.sample_url;
            @return.tags = current.tags;
            @return.filename = String.Concat(current.file_url.Reverse().TakeWhile(a => !a.Equals('/')).Reverse());
            @return.sourceSite = "Konachan";
            return @return;
        }

        protected override string GetURL(string tag, int pageNumber)
        {
            return "http://konachan.com/post.json?page=" + (pageNumber + 1).ToString() + "&tags=" + tag;
        }

        protected override List<AbstractGrabbedData> ProcessJsonData(byte[] json)
        {
            string str = Encoding.UTF8.GetString(json);
            dynamic d = Json.Decode(str, typeof(Konachan[]));
            if (d == null || d.Length == 0) return null;
            object[] temp = (object[])d;

            return temp.Select(a => (AbstractGrabbedData)((Konachan)a)).ToList();
        }
    }
}
