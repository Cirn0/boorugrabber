﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooruGrabber.Util.GrabbedData;
using BooruGrabber.Util;
using System.Net;
using System.Web.Helpers;
using System.Collections.ObjectModel;
using System.Security.Cryptography;
using System.IO;
using System.Windows.Media.Imaging;
using System.Threading;
using System.Text.RegularExpressions;

namespace BooruGrabber.Util.Grabber
{
    class ZerochanGrabber : AbstractHTMLGrabber
    {
        public override SimpleGrabbedFile DoConvertion(AbstractGrabbedData data)
        {
            try
            {
                Zerochan current = (Zerochan)data;
                SimpleGrabbedFile @return = new SimpleGrabbedFile();
                @return.id = current.id;
                @return.hash = "";
                @return.imagePath = current.imagePath;
                @return.previewPath = current.previewPath;
                @return.largerPreviewPath = current.largerPreviewPath;
                @return.tags = current.tags;
                @return.filename = current.filename;
                @return.sourceSite = "Zerochan";
                return @return;
            }
            catch
            {
                return null;
            }
        }

        public ZerochanGrabber()
        {
            
        }

        public override async Task<ObservableCollection<SimpleGrabbedFile>> GetImagesList(string tags, int threadsCount)
        {


            string firstTry = "http://www.zerochan.net/search?q=" + tags + "&s=id";

            HttpWebRequest hwr = WebRequest.CreateHttp(firstTry);
            hwr.Proxy = WebRequest.DefaultWebProxy;
            hwr.UserAgent = "Other";
            hwr.Method = WebRequestMethods.Http.Head;
            hwr.CookieContainer = GetCookieContainer();
            hwr.Referer = GetReferer();

            string link = String.Empty;
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    var response = (HttpWebResponse)hwr.GetResponse();
                    if (response.ResponseUri.ToString() != firstTry)
                    {
                        link = response.ResponseUri.ToString();
                    }
                    break;
                }
                catch (Exception)
                {
                    //repeat
                }
            }
            if (link == String.Empty)
            {
                return null;
            }

            await ThreadingUtil.StartNew(link, threadsCount, ImagesListLoadingTask);

            ObservableCollection<SimpleGrabbedFile> result = new ObservableCollection<SimpleGrabbedFile>();
            files.ForEach(a =>
            {
                SimpleGrabbedFile s = DoConvertion(a);
                if (s != null)
                    result.Add(s);
            });
            files.Clear();
            _imagesCount = result.Count;

            return result;
        }

        protected override void ImagesListLoadingTask(object _url)
        {
            while (9 == 9)
            {
                if (noMore)
                {
                    return;
                }
                string url = (string)_url;
                lock (filesIterationIndexLock)
                {
                    url = GetURL(url, filesIterationIndex);
                    filesIterationIndex++;
                }

                byte[] res = DownloadDataFromURL(url);
                string html = Encoding.UTF8.GetString(res);
                //А теперь давайте помолимся, чтобы на зерочане внезапно слегка не изменился движок и не поломал мне всю эту байду строкой ниже
                //Для примера приведу кусок кода страницы, который мы должны будем найти этой регуляркой:

                //<li >
                //<a href="/1908981" tabindex="1" style="width: 242px; "><span>8 Fav</span><img
                //src="http://s3.zerochan.net/Makise.Kurisu.240.1908981.jpg"
                //alt="Makise Kurisu" title="2000x1333 1832kB"
                //style="width: 240px; height: 160px; " /></a>
                //<p>
                //</p>
                //</li>

                Regex regex = new Regex(@"<li[>\s](?:(?!<\/li).)+<a href=\""\/(?<link>\d*?)\"".*?<img.*?src=\""(?<image>[^\""]*?)\"".*?<\/li>", RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
                var v = regex.Matches(html);

                if (v == null || v.Count == 0)
                {
                    noMore = true;
                    return;
                }

                List<Zerochan> tempFiles = new List<Zerochan>();
                foreach (Match match in v)
                {
                    int picID;
                    bool isParsed = Int32.TryParse(match.Groups["link"].Value, out picID);
                    if (!isParsed)
                    {
                        continue;
                    }
                    string previewPic = match.Groups["image"].Value;
                    //Не совсем уверен в следующей строчке. Как я понял, для превьюшек имя изображения всегда "тег.240.ID_картинки.расширение". Но абсолютной уверенности в этом нет.
                    string fullPic = previewPic.Replace(".240.", ".full.");
                    Uri uri = new Uri(fullPic);
                    string filename = String.Empty;
                    filename = uri.LocalPath.Trim('/');
                    tempFiles.Add(new Zerochan()
                    {
                        filename = filename,
                        id = picID,
                        imagePath = fullPic,
                        largerPreviewPath = fullPic,
                        previewPath = previewPic,
                        //не нашёл, как достать теги из превьюшки - для этого надо переходить на страницу с картинкой и парсить оттуда. А это долго и непродуктивно. Поэтому пока вместо тега показываем тег из названия картинки и ID изображения
                        tags = String.Concat(filename.Split('.').Reverse().Skip(3).Reverse().Select(a => a + ' ').Concat(new string[] { picID.ToString() }))
                    });
                }
                int сnt;
                lock (files)
                {
                    сnt = tempFiles.Except(files).Count(a => a != null);
                }
                _imagesCount += сnt;
                if (сnt == 0)     //может быть, что с сайта нам возвращается не пустой набор данных, а последний доступный. Для этого и проверяем
                {
                    noMore = true;
                    return;
                }
                lock (files)
                {
                    files.AddRange(tempFiles);
                }
            }
        }

        private string GetURL(string url, int filesIterationIndex)
        {
            return url + "?p=" + (filesIterationIndex+1);
        }

        //поскольку я решил не парсить страницы с каждой картинкой по отдельности, придётся слегка переписать процедуру загрузки изображения. Вкратце: у зерочана для пикч есть несколько префиксов (s1-s4), и превьюшка с одного из них может бтыь доступна, а основная картинка - нет. Для этого и нужны дополнительные проверки
        protected override byte[] DownloadDataFromURL(string url, SimpleGrabbedFile file)
        {
            string localUrl = url;
            HttpWebRequest hwr = null;
            var v = ProxyHelper.ProxyList.GetProxy();
            byte[] result = null;
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    hwr = WebRequest.CreateHttp(localUrl);
                    hwr.Proxy = (v == null) ? WebRequest.DefaultWebProxy : new WebProxy(v.proxyAddress, v.port);
                    hwr.UserAgent = "Other";
                    hwr.Method = WebRequestMethods.Http.Get;
                    hwr.CookieContainer = GetCookieContainer();
                    hwr.Referer = GetReferer();
                    hwr.Timeout = 10000;
                    using (var response = hwr.GetResponse())
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            //MemoryStream sr = new MemoryStream();
                            result = GetContentWithProgressReporting(stream, response.ContentLength, file);
                            //result = sr.ToArray();
                            ProxyHelper.ProxyList.ReportSucceededProxy(v);
                        }
                        response.Close();
                        break;
                    }
                }
                catch (WebException ex)
                {
                    hwr.Abort();
                    if (ex.Status == WebExceptionStatus.ProtocolError &&
                        ex.Response != null)
                    {
                        var resp = (HttpWebResponse)ex.Response;
                        if (resp.StatusCode == HttpStatusCode.NotFound)
                        {
                            Task<string>[] list = new Task<string>[4];
                            for (int i_ = 0; i_ < 4; i_++)
                            {
                                int tempo = i_;
                                list[i_] = Task.Factory.StartNew(() => 
                                {
                                    try
                                    {
                                        //регекс здесь, честно говоря, нафиг не нужен, но я просто не люблю String.Replace, String.IndexOf и подобное
                                        Regex reg = new Regex(@"http[s]*:\/\/s(\d)");
                                        string url_ = localUrl;
                                        url_ = reg.Replace(url_, "http://s" + (tempo + 1).ToString());

                                        HttpWebRequest hwr_ = WebRequest.CreateHttp(url_);
                                        var v_ = ProxyHelper.ProxyList.GetProxy();
                                        hwr_.Proxy = (v_ == null) ? WebRequest.DefaultWebProxy : new WebProxy(v_.proxyAddress, v_.port);
                                        hwr_.UserAgent = "Other";
                                        hwr_.Timeout = 1000;
                                        hwr_.Method = WebRequestMethods.Http.Head;
                                        hwr_.CookieContainer = GetCookieContainer();
                                        hwr_.Referer = GetReferer();
                                        var response_ = hwr_.GetResponse();
                                        response_.Close();
                                        response_.Dispose();
                                        return url_;
                                    }
                                    catch
                                    {
                                        return "";
                                    }
                                });
                            }
                            var va = Task.WhenAll(list).Result;
                            try
                            {
                                try
                                {
                                    string url__ = va.First(a => a != "");

                                    result = DownloadDataFromURL(url__, file);
                                }
                                catch
                                {
                                    result = null;
                                }
                                break;
                            }
                            catch
                            {
                                //не получили ни одного адекватного ответа - значит, отовсюду недоступна. Дальше пытаться нет смыла
                                break;
                            }
                        }
                    }
                    UpdateCookie();
                    ProxyHelper.ProxyList.ReportFailedProxy(v);
                }
                catch (Exception)
                {
                    //repeat
                }
                finally
                {
                    hwr.Abort();
                }
            }

            return result;
        }

        protected override void ImagesDownloadingTask(object _folderPath)
        {
            while (9 == 9)
            {
                if (noMore)
                {
                    return;
                }
                string folderPath = (string)_folderPath;
                SimpleGrabbedFile currentFile = null;
                lock (filesIterationIndexLock)
                {
                    if (filesToDownload.Count <= filesIterationIndex)
                    {
                        noMore = true;
                        return;
                    }
                    currentFile = filesToDownload[filesIterationIndex];
                    filesIterationIndex++;
                }

                byte[] b = null;
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        b = DownloadFile_(currentFile);
                        if (DownloadingSettings.settings.saveTagsInFileNames)
                        {
                            byte[] pageWithTags = DownloadDataFromURL("http://zerochan.net/" + currentFile.id.ToString());
                            string content = Encoding.UTF8.GetString(pageWithTags);
                            Regex insideUL = new Regex(@"<ul[^>]+tags[^>]*>(.*?)<\/ul>", RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
                            Match match1 = insideUL.Match(content);
                            string trimmedContent = match1.Captures[0].Value;
                            Regex allTags = new Regex(@"<li[^>]*><a[^>]*>(?<tag>.*?)<\/a[^>]*>.*?<\/li[^>]*>", RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
                            var match2 = allTags.Matches(trimmedContent);
                            string oldFilename = currentFile.filename;
                            currentFile.filename = String.Empty;
                            foreach (Match mtch in match2)
                            {
                                currentFile.filename += mtch.Groups["tag"].Value + " ";
                            }
                            currentFile.filename.TrimEnd();
                            currentFile.filename += Path.GetExtension(oldFilename);
                        }
                        if (CompareHash(b, currentFile.hash))
                            break;
                        else
                            b = null;
                    }
                    catch (WebException)
                    {
                        //repeat
                    }
                }
                if (b == null)
                    return;

                WriteFile(currentFile, folderPath, b);

                _downloadedCount += 1;
            }
        }

        protected override byte[] DownloadDataFromURL(string url)
        {
            SimpleGrabbedFile file = new SimpleGrabbedFile();
            return DownloadDataFromURL(url, file);
        }
    }
}
