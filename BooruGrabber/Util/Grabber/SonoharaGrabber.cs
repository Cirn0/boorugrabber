﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooruGrabber.Util.GrabbedData;
using System.Net;
using System.Web.Helpers;
using System.Security.Cryptography;
using System.Windows;

namespace BooruGrabber.Util.Grabber
{
    class SonoharaGrabber: AbstractJsonGrabber
    {
        public SonoharaGrabber()
        {
            hashAlgorithm = new MD5CryptoServiceProvider();
        }

        public override SimpleGrabbedFile DoConvertion(AbstractGrabbedData data)
        {
            try
            {
                Sonohara current = (Sonohara)data;
                SimpleGrabbedFile @return = new SimpleGrabbedFile();
                @return.id = current.id;
                @return.hash = current.md5;
                @return.imagePath = "http://Sonohara.donmai.us" + current.file_url;
                @return.previewPath = "http://Sonohara.donmai.us" + current.preview_file_url;
                @return.largerPreviewPath = "http://Sonohara.donmai.us" + ((current.has_large ?? false) ? current.large_file_url : current.preview_file_url);
                @return.tags = current.tag_string;
                @return.filename = String.Concat(current.file_url.Reverse().TakeWhile(a => !a.Equals('/')).Reverse());
                @return.sourceSite = "Sonohara";
                return @return;
            }
            catch
            {
                return null;
            }
        }

        protected override string GetURL(string tag, int pageNumber)
        {
            return "http://Sonohara.donmai.us/posts.json?limit=100&page=" + (pageNumber + 1).ToString() + "&tags=" + tag;
        }

        protected override List<AbstractGrabbedData> ProcessJsonData(byte[] json)
        {
            string str = Encoding.UTF8.GetString(json);
            dynamic d = Json.Decode(str, typeof(Sonohara[]));
            if (d == null || d.Length == 0) return null;
            object[] temp = (object[])d;

            return temp.Select(a => ((((Sonohara)a).is_banned ?? false) || (((Sonohara)a).is_deleted ?? false)) ? null : (AbstractGrabbedData)((Sonohara)a)).ToList();
        }
    }
}
