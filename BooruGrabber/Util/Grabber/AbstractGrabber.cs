﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooruGrabber.Util.GrabbedData;
using BooruGrabber.Util;
using System.Net;
using System.Web.Helpers;
using System.Collections.ObjectModel;
using System.Security.Cryptography;
using System.IO;
using System.Windows.Media.Imaging;
using System.Threading;

namespace BooruGrabber.Util.Grabber
{
    public abstract class AbstractGrabber
    {
        public SimpleGrabbedFile currentlyProcessedFile
        {
            get
            {
                return _currentlyProcessedFile;
            }
        }
        protected SimpleGrabbedFile _currentlyProcessedFile = null;

        protected ObservableCollection<SimpleGrabbedFile> filesToDownload = null;

        //для чтения данных снаружи
        public int imagesCount
        {
            get
            {
                return _imagesCount;
            }
        }
        //для изменения данных изнутри
        protected int _imagesCount
        {
            get
            {
                return __imagesCount;
            }
            set
            {
                lock (imagesCounterLock)
                {
                    __imagesCount = value;
                }
            }
        }
        //руками не трогать
        private int __imagesCount;
        protected object imagesCounterLock = new object();

        public int downloadedCount
        {
            get
            {
                return _downloadedCount;
            }
        }
        protected int _downloadedCount
        {
            get
            {
                return __downloadedCount;
            }
            set
            {
                lock (downloadedCounterLock)
                {
                    __downloadedCount = value;
                }
            }
        }
        protected int __downloadedCount;      //we must go deeper...
        protected object downloadedCounterLock = new object();

        protected object obj = new object();
        public HashAlgorithm hashAlgorithm;
        protected NetworkCredential credentials = null;

        protected List<AbstractGrabbedData> files = new List<AbstractGrabbedData>();

        protected int filesIterationIndex;
        protected Object filesIterationIndexLock = new object();

        protected bool noMore = false;
        protected bool needCreateThreads = true;

        public async Task<byte[]> DownloadFile(SimpleGrabbedFile file)
        {
            byte[] result = await Task.Factory.StartNew(() => DownloadDataFromURL(file.imagePath));
            file.downloadingPercent = 100;      //костыль
            return result;
        }

        public byte[] DownloadFile_(SimpleGrabbedFile file)
        {
            byte[] result = DownloadingSettings.settings.useAnonymizer ? Anonymizer.LoadDataThrough(file.imagePath, file) : DownloadDataFromURL(file.imagePath, file);
            file.downloadingPercent = 100;
            return result;
        }

        public async Task<byte[]> DownloadPreview(SimpleGrabbedFile file)
        {
            return await Task.Factory.StartNew(() => DownloadingSettings.settings.useAnonymizer ? Anonymizer.LoadDataThrough(file.previewPath) : DownloadDataFromURL(file.previewPath));
        }

        public byte[] DownloadLargerPreview(SimpleGrabbedFile file)
        {
            return DownloadingSettings.settings.useAnonymizer ? Anonymizer.LoadDataThrough(file.largerPreviewPath) : DownloadDataFromURL(file.largerPreviewPath);
        }

        //передача по ссылке в асинхронные методы невозможна, а мне нужна именно ссылка на коллекцию - потому что я буду менять данные о проценте загрузки. Поэтому вынес это в отдельный метод
        public void AssignObservableCollection(ref ObservableCollection<SimpleGrabbedFile> files_)
        {
            filesToDownload = files_;
        }

        public virtual async Task<ObservableCollection<SimpleGrabbedFile>> GetImagesList(string tags, int threadsCount)
        {
            filesIterationIndex = 0;
            noMore = false;
            await ThreadingUtil.StartNew(tags, threadsCount, ImagesListLoadingTask);

            ObservableCollection<SimpleGrabbedFile> result = new ObservableCollection<SimpleGrabbedFile>();
            files.ForEach(a =>
            {
                SimpleGrabbedFile s = DoConvertion(a);
                if (s != null)
                    result.Add(s);
            });
            files.Clear();
            _imagesCount = result.Count;

            return result;
        }

        protected abstract void ImagesListLoadingTask(object _tag);

        public abstract SimpleGrabbedFile DoConvertion(AbstractGrabbedData data);

        public async Task DownloadImages(string folderPath, int threadsCount)
        {
            filesIterationIndex = 0;
            noMore = false;
            await ThreadingUtil.StartNew(folderPath, threadsCount, ImagesDownloadingTask);
        }

        protected virtual void ImagesDownloadingTask(object _folderPath)
        {
            while (9 == 9)
            {
                if (noMore)
                {
                    return;
                }
                string folderPath = (string)_folderPath;
                SimpleGrabbedFile currentFile = null;
                lock (filesIterationIndexLock)
                {
                    if (filesToDownload.Count <= filesIterationIndex)
                    {
                        noMore = true;
                        return;
                    }
                    currentFile = filesToDownload[filesIterationIndex];
                    filesIterationIndex++;
                }

                _currentlyProcessedFile = currentFile;

                byte[] b = null;
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        b = DownloadFile_(currentFile);
                        if (CompareHash(b, currentFile.hash))
                            break;
                        b = null;
                    }
                    catch (WebException)
                    {
                        //repeat
                    }
                }
                if (b == null)
                {
                    return;
                }

                WriteFile(currentFile, folderPath, b);

                _downloadedCount += 1;
            }
        }

        protected bool IsHashFound(byte[] content)
        {
            //если для сайта не указан свой алгоритм хэширования, то мы используем тот же, который использовали для хэширования картинок в папке
            HashAlgorithm algo = (hashAlgorithm ?? new MD5CryptoServiceProvider());
            string hash = BitConverter.ToString(algo.ComputeHash(content)).Replace("-", "");

            return GrabberUtil.isHashFound(hash);
        }

        protected virtual void WriteFile(SimpleGrabbedFile currentFile, string folderPath, byte[] content)
        {
            if (IsHashFound(content))
            {
                return;
            }

            string filename = new String(currentFile.filename.TakeWhile(a => a != '?').ToArray());
            Path.GetInvalidFileNameChars().ToList().ForEach(a => filename = filename.Replace(String.Concat(a), ""));

            string ext = Path.GetExtension(folderPath + "\\" + filename);

            currentFile.filename = filename;


            string fileName;

            try
            {
                fileName = GenerateFileName(folderPath, currentFile, ext);
            }
            catch (PathTooLongException)
            {
                Log.log.AddEntry(Log.MessageType.warning, String.Format("Не удаётся сохранить файл {0}. Возможно, путь к папке для сохранения слишком длинный.", currentFile.filename));
                return;
            }

            File.WriteAllBytes(fileName, content);
        }

        protected virtual string GenerateFileName(string folderPath, SimpleGrabbedFile currentFile, string ext, bool unique = true)
        {
            string extension = ext;
            int fileIndex = 0;
            string filename = new String(currentFile.filename.TakeWhile(a => a != '?').ToArray());
            string fileName = String.Empty;
            bool? isFileAlreadyExists = null;
            while (isFileAlreadyExists ?? true)
            {
                if (fileIndex != 0)
                {
                    extension = String.Format(" ({0})", fileIndex) + ext;
                }
                if (folderPath.Length + extension.Length + 1 >= 259)
                {
                    throw new PathTooLongException();
                }
                fileName = (DownloadingSettings.settings.saveTagsInFileNames && DownloadingSettings.settings.allowSaveTagsInFileName && currentFile.tags != String.Empty) ?
                            folderPath +
                            "\\" +
                            String.Concat(filename.Reverse()
                                            .SkipWhile(a => !a.Equals('.'))
                                            .Skip(1)
                                            .Reverse()
                                            .Concat(new char[] { ' ' })
                                            .Concat(currentFile.tags.Where(a => !Path.GetInvalidFileNameChars().Contains(a)))
                                            .Take(259 - (folderPath + "\\").Length - extension.Length)
                                            .Concat(extension)) :
                            folderPath + "\\" + Path.GetFileNameWithoutExtension(filename) + extension;

                if (new FileInfo(fileName).Exists && unique)
                {
                    isFileAlreadyExists = true;
                    fileIndex++;
                }
                else
                {
                    isFileAlreadyExists = false;
                }
            }

            return fileName;
        }

        protected abstract bool CompareHash(byte[] filesData, string hash);

        protected virtual byte[] DownloadDataFromURL(string url, SimpleGrabbedFile file)
        {
            HttpWebRequest hwr = WebRequest.CreateHttp(url);
            var v = ProxyHelper.ProxyList.GetProxy();
            hwr.Proxy = (v == null) ? WebRequest.DefaultWebProxy : new WebProxy(v.proxyAddress, v.port);
            hwr.UserAgent = "Other";
            hwr.Method = WebRequestMethods.Http.Get;
            hwr.CookieContainer = GetCookieContainer();
            hwr.Referer = GetReferer();

            byte[] result = null;
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    using (var response = hwr.GetResponse())
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            //MemoryStream sr = new MemoryStream();
                            result = GetContentWithProgressReporting(stream, (response.ContentLength != -1) ? response.ContentLength : 0, file);
                            //result = sr.ToArray();
                            ProxyHelper.ProxyList.ReportSucceededProxy(v);
                        }
                    }
                    break;
                }
                catch (WebException)
                {
                    UpdateCookie();
                    ProxyHelper.ProxyList.ReportFailedProxy(v);
                    v = ProxyHelper.ProxyList.GetProxy();
                    hwr.Proxy = (v == null) ? WebRequest.DefaultWebProxy : new WebProxy(v.proxyAddress, v.port);
                }
                catch (Exception)
                {
                    //repeat
                }
            }

            return result;
        }

        protected virtual byte[] DownloadDataFromURL(string url)
        {
            byte[] result = null;
            HttpWebRequest hwr = null;
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    hwr = WebRequest.CreateHttp(url);
                    hwr.Proxy = WebRequest.DefaultWebProxy;
                    hwr.UserAgent = "Other";
                    hwr.Method = WebRequestMethods.Http.Get;
                    hwr.CookieContainer = GetCookieContainer();
                    hwr.Referer = GetReferer();
                    using (var response = hwr.GetResponse())
                    {
                        using (MemoryStream sr = new MemoryStream())
                        {
                            response.GetResponseStream().CopyTo(sr);
                            result = sr.ToArray();
                        }
                        response.Close();
                    }
                    break;
                }
                catch (WebException)
                {
                    UpdateCookie();
                }
                catch (Exception)
                {
                    //repeat
                }
                finally
                {
                    if (result == null)
                    {
                        Log.log.AddEntry(Log.MessageType.warning, String.Format("Попытка {0} из 5. Запрос завершён с ошибкой.", i + 1));
                    }
                    hwr?.Abort();
                }
            }

            return result;
        }

        //на самом деле, намного логичнее было бы передавать сюда по ссылке не весь сграбленный файл, а только downloadingPercent. Но фреймворк не даёт передавать по ссылке свойства, а переменную передавать смысла нет - грид обновляться не будет. Поэтому сделал вот так
        protected byte[] GetContentWithProgressReporting(Stream responseStream, long contentLength, SimpleGrabbedFile file)
        {
            file.SetDownloadingPercent(0);
            byte[] data;
            if (contentLength > 0)
            {
                data = new byte[contentLength];
                int currentIndex = 0;
                int buffSize = (int)(contentLength / 20);
                var buffer = new byte[buffSize];
                do
                {
                    //var bytesReceived = responseStream.Read(data, currentIndex, buffSize);
                    var bytesReceived = responseStream.Read(buffer, 0, buffSize);
                    Array.Copy(buffer, 0, data, currentIndex, bytesReceived);
                    currentIndex += bytesReceived;

                    // Report percentage
                    double percentage = (double)currentIndex / contentLength;
                    file.SetDownloadingPercent(percentage * 100);
                } while (currentIndex < contentLength);
            }
            else
            {
                //в таком случае сервер нам не передал contentLength. Выкручиваемся как можем
                file.SetDownloadingPercent(-1);
                using (MemoryStream sr = new MemoryStream())
                {
                    responseStream.CopyTo(sr);
                    sr.Position = 0;
                    data = new byte[sr.Length];
                    sr.Read(data, 0, data.Length);
                }
            }

            file.SetDownloadingPercent(100);

            return data;
        }

        protected virtual CookieContainer GetCookieContainer()
        {
            return new CookieContainer();
        }

        protected virtual void UpdateCookie() { }

        protected virtual string GetReferer()
        {
            return String.Empty;
        }

        public void AbortCurrentOperation()
        {
            noMore = true;
        }
    }
}
