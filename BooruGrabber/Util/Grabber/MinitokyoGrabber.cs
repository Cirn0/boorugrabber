﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooruGrabber.Util.GrabbedData;
using BooruGrabber.Util;
using System.Net;
using System.Web.Helpers;
using System.Collections.ObjectModel;
using System.Security.Cryptography;
using System.IO;
using System.Windows.Media.Imaging;
using System.Threading;
using System.Text.RegularExpressions;
using System.Security;

namespace BooruGrabber.Util.Grabber
{
    class MinitokyoGrabber : AbstractHTMLGrabber
    {
        private int currentImagesType = 0;
        private bool faggotsReturns404 = false;

        private CookieContainer container = null;

        private string GetImageTypeName()
        {
            switch (currentImagesType)
            {
                case 1:
                    {
                        return "Wallpapers";
                    } 
                //break;
                case 2:
                    {
                        return "Indy-Art";
                    }
                //break;
                case 3:
                    {
                        return "Scans";
                    }
                //break;
                default:
                    {
                        return "";
                    }
                //break;
            }
        }

        public override SimpleGrabbedFile DoConvertion(AbstractGrabbedData data)
        {
            try
            {
                Minitokyo current = (Minitokyo)data;
                SimpleGrabbedFile @return = new SimpleGrabbedFile();
                @return.id = current.id;
                @return.hash = "";
                @return.imagePath = current.imagePath;
                @return.previewPath = current.previewPath;
                @return.largerPreviewPath = current.largerPreviewPath;
                @return.tags = current.tags;
                @return.filename = current.filename;
                @return.sourceSite = "Minitokyo";
                return @return;
            }
            catch
            {
                return null;
            }
        }

        public override async Task<ObservableCollection<SimpleGrabbedFile>> GetImagesList(string tags, int threadsCount)
        {
            string firstTry = "http://www.minitokyo.net/search?q=" + tags.Replace('_', ' ');        //Eсли не заменить _ на пробелы, сервер возвращает 404.
            
            string tagID = String.Empty;
            string link = String.Empty;
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    try
                    {
                        HttpWebRequest hwr = WebRequest.CreateHttp(firstTry);
                        hwr.Proxy = WebRequest.DefaultWebProxy;
                        hwr.UserAgent = "Other";
                        hwr.Method = WebRequestMethods.Http.Get;
                        hwr.CookieContainer = GetCookieContainer();
                        hwr.Referer = GetReferer();
                        hwr.Host = "www.minitokyo.net";
                        hwr.Timeout = 10000;
                        var response = (HttpWebResponse)hwr.GetResponse();
                        if (response.ResponseUri.ToString() != firstTry)
                        {
                            link = response.ResponseUri.ToString();

                            using (Stream resp = response.GetResponseStream())
                            {
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    resp.CopyTo(stream);
                                    byte[] mainPage_ = stream.ToArray();
                                    string mainPage = Encoding.UTF8.GetString(mainPage_);
                                    Regex regex = new Regex(@"<p\s+class=""more"">\s+<a\s+href=""(?:.*?\/?tid=(?<id>\d+).*?)", RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
                                    Match m = regex.Match(mainPage);
                                    tagID = m.Groups["id"].Value;
                                    currentImagesType = 1;
                                }
                            }
                        }
                        else
                        {
                            //Значит, в точности такого тега нет. Например, для поиска тега "makise_kurisu" использовали "kurisu" или даже "*kurisu*". В этом случае сайт не переходит на специальную страницу для этого тега, а просто выводит все подходящие картинки на нескольких страницах. 
                            //Основная проблема в том, что сайт написан таким образом, что в случае отсутствия полного соответствия сайт показывает все подходящие картинки, да. Но при этом он почему-то возвращает ошибку 404. Поэтому в этот блок мы, в теории, зайти не должны. Но мало ли, как всё в будущем изменится - поэтому оставлю.
                            link = response.ResponseUri.ToString();
                            tagID = firstTry;
                        }
                        break;
                    }
                    catch (WebException e)
                    {
                        if (e.Status == WebExceptionStatus.ProtocolError &&
                        e.Response != null)
                        {
                            var resp = (HttpWebResponse)e.Response;
                            if (resp.StatusCode == HttpStatusCode.NotFound)
                            {
                                link = resp.ResponseUri.ToString();
                                tagID = firstTry;
                                faggotsReturns404 = true;
                                break;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    //repeat
                }
            }
            if (link == String.Empty)
            {
                return null;
            }

            await ThreadingUtil.StartNew(tagID, /*threadsCount*/1, ImagesListLoadingTask);      //там работа в несколько потоков сложнее, чем в остальных случаях. Пока не нашёл как пофиксить, поэтому просто жёстко выставляю количество потоков для скачивания списка изображений в 1

            ObservableCollection<SimpleGrabbedFile> result = new ObservableCollection<SimpleGrabbedFile>();
            files.ForEach(a =>
            {
                SimpleGrabbedFile s = DoConvertion(a);
                if (s != null)
                    result.Add(s);
            });
            files.Clear();
            _imagesCount = result.Count;

            return result;
        }

        protected override void ImagesListLoadingTask(object _tagID)
        {
            while (9 == 9)
            {
                if (noMore)
                {
                    return;
                }
                int unused;
                bool isTagFound = Int32.TryParse(_tagID.ToString(), out unused);

                string tagID = (string)_tagID;
                string url;
                string typeName;
                lock (filesIterationIndexLock)
                {
                    if (isTagFound)
                    {
                        url = GetURL(tagID, filesIterationIndex);
                    }
                    else
                    {
                        url = GetURLForSearch(tagID, filesIterationIndex);
                    }
                    filesIterationIndex++;
                    typeName = GetImageTypeName();
                }

                byte[] result = DownloadDataFromURL(url);
                string html = Encoding.UTF8.GetString(result);

                Regex regex = new Regex(@"<li[^>]*>\s*?<a href=\""(?<link>[^""]*?(?<ID>\d*))\""[^>]*?>(?:<em[^>]+?><\/em>){0,1}<img[^>]*?src=\""(?<image>[^""]*?)\""", RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
                var v = regex.Matches(html);

                if (v == null || v.Count == 0)
                {
                    if (!isTagFound)
                    {
                        noMore = true;
                        return;
                    }
                    //TODO: проверить, что будет в случае многопоточности - потеряются ли картинки, задвоятся (хотя этого точно не будет) или ещё что произойдёт
                    filesIterationIndex = 0;
                    currentImagesType++;
                    if (currentImagesType > 3)
                    {
                        noMore = true;
                        return;
                    }
                    continue;
                }

                List<Minitokyo> tempFiles = new List<Minitokyo>();
                foreach (Match match in v)
                {
                    int picID;
                    bool isParsed = Int32.TryParse(match.Groups["ID"].Value, out picID);
                    if (!isParsed)
                    {
                        continue;
                    }
                    string previewPic = match.Groups["image"].Value.Replace("/view/", "/thumbs/");
                    string fullPic = previewPic.Replace("/thumbs/", "/downloads/").Replace("/view/", "/downloads");
                    string largerPreviewPic = previewPic.Replace("/thumbs/", "/view/");
                    Uri uri = new Uri(fullPic);
                    string filename = String.Empty;
                    filename = uri.Segments.Last();     //Теоретически там должно быть имя файла, но я не проверял на большой выборке - может быть то, что после знака "?", так что надо быть аккуратнее
                    tempFiles.Add(new Minitokyo()
                    {
                        filename = filename,
                        id = picID,
                        imagePath = fullPic,
                        largerPreviewPath = largerPreviewPic,
                        previewPath = previewPic,
                        //Теги отсюда не выцепляются, поэтому показываем ID изображения и его тип
                        tags = typeName + ", " + picID.ToString()
                    });
                }
                int сnt;
                lock (files)
                {
                    сnt = tempFiles.Except(files).Where(a => a != null).Count();
                }
                _imagesCount += сnt;
                if (сnt == 0)     //может быть, что с сайта нам возвращается не пустой набор данных, а последний доступный. Для этого и проверяем
                {
                    if (!isTagFound)
                    {
                        noMore = true;
                        return;
                    }
                    filesIterationIndex = 0;
                    currentImagesType++;
                    if (currentImagesType > 3)
                    {
                        noMore = true;
                        return;
                    }
                    continue;
                }
                lock (files)
                {
                    files.AddRange(tempFiles);
                }
            }
        }

        private string GetURL(string tagID, int filesIterationIndex)
        {
            return String.Format(@"http://browse.minitokyo.net/gallery?tid={0}&index={1}&page={2}", tagID, currentImagesType, filesIterationIndex + 1);
        }

        private string GetURLForSearch(string searchLink, int filesIterationIndex)
        {
            return searchLink + String.Format(@"&page={0}", filesIterationIndex + 1);
        }

        protected override void ImagesDownloadingTask(object _folderPath)
        {
            while (9 == 9)
            {
                if (noMore)
                {
                    return;
                }
                string folderPath = (string)_folderPath;
                SimpleGrabbedFile currentFile = null;
                lock (filesIterationIndexLock)
                {
                    if (filesToDownload.Count <= filesIterationIndex)
                    {
                        noMore = true;
                        return;
                    }
                    currentFile = filesToDownload[filesIterationIndex];
                    filesIterationIndex++;
                }

                byte[] b = null;
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        b = DownloadFile_(currentFile);
                        if (DownloadingSettings.settings.saveTagsInFileNames)
                        {
                            try
                            {
                                byte[] pageWithTags = DownloadDataFromURL("http://gallery.minitokyo.net/view/" + currentFile.id.ToString());
                                string content = Encoding.UTF8.GetString(pageWithTags);
                                Regex insideUL = new Regex(@"<div id=""tag-list"">.*?<ul[^>]*>(.*?)<\/ul>", RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
                                Match match1 = insideUL.Match(content);
                                string trimmedContent = match1.Captures[0].Value;
                                Regex allTags = new Regex(@"<li[^>]*?>.*?<a[^>]*>(?<tag>[^<>]+?)<\/a>.*?<\/li>", RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
                                var match2 = allTags.Matches(trimmedContent);
                                string oldFilename = currentFile.filename;
                                currentFile.filename = String.Empty;
                                foreach (Match mtch in match2)
                                {
                                    currentFile.filename += mtch.Groups["tag"].Value + " ";
                                }
                                currentFile.filename.TrimEnd(' ');
                                currentFile.filename += Path.GetExtension(oldFilename);
                                currentFile.filename = currentFile.id.ToString() + " " + currentFile.filename;
                            }
                            catch       //скорее всего, не смогли найти соответствие регуляркой. Бывает. В таком случае забиваем и сохраняем файл как положено
                            {

                            }
                        }
                        if (CompareHash(b, currentFile.hash))
                            break;
                        else
                            b = null;
                    }
                    catch (WebException)
                    {
                        //repeat
                    }
                }
                if (b == null)
                    continue;

                WriteFile(currentFile, folderPath, b);

                _downloadedCount += 1;
            }
        }

        protected override byte[] DownloadDataFromURL(string url)
        {
            SimpleGrabbedFile file = new SimpleGrabbedFile();
            return DownloadDataFromURL(url, file);
        }

        protected override CookieContainer GetCookieContainer()
        {
            //Здесь нам немного не подходит методика с санкаку, потому что пароль должен будет храниться в открытом виде. Поэтому метод немного переделаем - куки задаём один раз и снаружи класса, а потом уже постоянно используем один и тот же контейнер
            if (container == null)
                //UpdateCookie();
                return container = new CookieContainer();

            return container;
        }

        public void UpdateCookie(string login, SecureString password)
        {
            //Честно говоря, мало смысла прятать пароль. С учётом того, что после этого он пойдёт по незашифрованному каналу. Но всё равно я должен сделать всё, что в моих силах
            HttpWebRequest hwr = WebRequest.CreateHttp("http://my.minitokyo.net/login");
            hwr.Proxy = WebRequest.DefaultWebProxy;
            hwr.UserAgent = "Other";
            hwr.Method = WebRequestMethods.Http.Post;
            hwr.PreAuthenticate = true;
            hwr.AllowAutoRedirect = true;
            hwr.Referer = "http://my.minitokyo.net/login";
            hwr.Host = "my.minitokyo.net";
            hwr.ContentType = "application/x-www-form-urlencoded";

            byte[] array = new byte["username=&password=&login=Login".Length + login.Length + password.Length];
            Encoding.UTF8.GetBytes(String.Format("username={0}&password=", login)).CopyTo(array, 0);

            byte[] arr = DataProtection.ReadDataFromSecureString(password);
            for (int i = 0; i < arr.Length; i++)
            {
                array[i + String.Format("username={0}&password=", login).Length] = arr[i];
            }
            DataProtection.ClearByteArray(ref arr);

            Encoding.UTF8.GetBytes("&login=Login").CopyTo(array, String.Format("username={0}&password=", login).Length + password.Length);
            
            hwr.ContentLength = array.Length;

            using (var stream = hwr.GetRequestStream())
            {
                stream.Write(array, 0, array.Length);
                stream.Flush();
            }

            DataProtection.ClearByteArray(ref array);

            hwr.CookieContainer = new CookieContainer();
            bool isFine = false;
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    var response = hwr.GetResponse();
                    isFine = true;
                    break;
                }
                catch (Exception)
                {
                    //repeat
                }
            }
            if (!isFine)
            {
                throw new CookieException();        //TODO: добавить кастомную ошибку
            }

            container = hwr.CookieContainer;
        }

        protected override void UpdateCookie()
        {
            throw new NotSupportedException();
        }

        //Y Minitokyo для пикч есть несколько префиксов (static, static1-static6), и превьюшка с одного из них может бтыь доступна, а основная картинка - нет. Для этого и нужны дополнительные проверки
        protected override byte[] DownloadDataFromURL(string url, SimpleGrabbedFile file)
        {
            string localUrl = url;
            HttpWebRequest hwr = null;
            var v = ProxyHelper.ProxyList.GetProxy();

            byte[] result = null;
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    hwr = WebRequest.CreateHttp(localUrl);
                    hwr.Proxy = (v == null) ? WebRequest.DefaultWebProxy : new WebProxy(v.proxyAddress, v.port);
                    hwr.UserAgent = "Other";
                    hwr.Method = WebRequestMethods.Http.Get;
                    hwr.CookieContainer = GetCookieContainer();
                    hwr.Referer = GetReferer();
                    hwr.Timeout = 10000;
                    using (var response = hwr.GetResponse())
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            result = GetContentWithProgressReporting(stream, response.ContentLength, file);
                            ProxyHelper.ProxyList.ReportSucceededProxy(v);
                        }
                        response.Close();
                        break;
                    }
                }
                catch (WebException ex)
                {
                    hwr.Abort();
                    if (ex.Status == WebExceptionStatus.ProtocolError &&
                        ex.Response != null)
                    {
                        var resp = (HttpWebResponse)ex.Response;
                        if (resp.StatusCode == HttpStatusCode.NotFound)
                        {
                            if (faggotsReturns404 && file.imagePath == null)      //то есть если мы одновременно и имеем подозрение, что нам могут вернуть 404, и качаем страницу, а не картинку (при скачивании картинки мы передаём адекватную ссылку на SimpleGrabbedFile, где будет отображаться прогресс)
                            {
                                try
                                {
                                    using (Stream resp_ = ex.Response.GetResponseStream())
                                    {
                                        using (MemoryStream stream = new MemoryStream())
                                        {
                                            resp_.CopyTo(stream);
                                            byte[] page = stream.ToArray();
                                            return page;
                                        }
                                    }
                                }
                                catch
                                {

                                }
                            }
                            else
                            {
                                //код дальше должен использоваться для загрузки картинок, которые вернут 404 только если реально недоступны. Так что там try..catch для 404 можно опустить
                                Task<string>[] list = new Task<string>[7];
                                for (int i_ = 0; i_ <= 6; i_++)
                                {
                                    int tempo = i_;
                                    list[i_] = Task.Factory.StartNew(() =>
                                    {
                                        try
                                        {
                                        //регекс здесь, честно говоря, нафиг не нужен, но я просто не люблю String.Replace, String.IndexOf и подобное
                                        Regex reg = new Regex(@"http[s]*:\/\/static(\d){0,1}");
                                            string url_ = localUrl;
                                            url_ = reg.Replace(url_, "http://static" + (tempo == 0 ? "" : (tempo + 1).ToString()));

                                            HttpWebRequest hwr_ = WebRequest.CreateHttp(url_);
                                            var v_ = ProxyHelper.ProxyList.GetProxy();
                                            hwr_.Proxy = (v_ == null) ? WebRequest.DefaultWebProxy : new WebProxy(v_.proxyAddress, v_.port);
                                            hwr_.UserAgent = "Other";
                                            hwr_.Timeout = 1000;
                                            hwr_.Method = WebRequestMethods.Http.Head;
                                            hwr_.CookieContainer = GetCookieContainer();
                                            hwr_.Referer = GetReferer();
                                            var response_ = hwr_.GetResponse();
                                            response_.Close();
                                            response_.Dispose();
                                            return url_;
                                        }
                                        catch
                                        {
                                            return "";
                                        }
                                    });
                                }
                                var va = Task.WhenAll(list).Result;
                                try
                                {
                                    try
                                    {
                                        string url__ = va.First(a => a != "");

                                        result = DownloadDataFromURL(url__, file);
                                    }
                                    catch
                                    {
                                        result = null;
                                    }
                                    break;
                                }
                                catch
                                {
                                    //не получили ни одного адекватного ответа - значит, отовсюду недоступна. Дальше пытаться нет смыла
                                    break;
                                }
                            }
                        }
                    }
                    ProxyHelper.ProxyList.ReportFailedProxy(v);
                }
                catch (Exception)
                {
                    //repeat
                }
                finally
                {
                    hwr.Abort();
                }
            }

            return result;
        }
    }
}