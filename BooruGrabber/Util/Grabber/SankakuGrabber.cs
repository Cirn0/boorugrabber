﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooruGrabber.Util.GrabbedData;
using System.Net;
using System.Web.Helpers;
using System.Security.Cryptography;
using System.Windows;
using System.Security;

namespace BooruGrabber.Util.Grabber
{
    class SankakuGrabber : AbstractJsonGrabber
    {
        private string authString;
        private Tuple<string, string> loginAndPasswordHash;

        private CookieContainer container = null;

        public SankakuGrabber(string login, SecureString password)
        {
            hashAlgorithm = new MD5CryptoServiceProvider();
            authString = AuthString(login, password);
            password.Clear();
        }

        public override SimpleGrabbedFile DoConvertion(AbstractGrabbedData data)
        {
            Sankaku current = (Sankaku)data;
            SimpleGrabbedFile @return = new SimpleGrabbedFile();
            @return.id = current.id;
            @return.hash = current.md5;
            @return.imagePath = "http:"+current.file_url;
            @return.previewPath = "http:" + current.preview_url;
            @return.largerPreviewPath = "http:" + current.sample_url;
            @return.tags = "";
            current.tags.ToList().ForEach(a =>  @return.tags += a.name + "; " );
            @return.filename = String.Concat(current.file_url.Reverse().TakeWhile(a => !a.Equals('/')).Reverse());
            @return.sourceSite = "Sankaku";
            return @return;
        }

        protected override string GetURL(string tag, int pageNumber)
        {
            return "https://chan.sankakucomplex.com/post/index.json?limit=100&page=" + (pageNumber+1).ToString() + "&tags=" + tag + authString;
        }

        protected override List<AbstractGrabbedData> ProcessJsonData(byte[] json)
        {
            string str = Encoding.UTF8.GetString(json);
            dynamic d = Json.Decode(str, typeof(Sankaku[]));
            if (d == null || d.Length == 0) return null;
            object[] temp = (object[])d;

            return temp.Select(a => (AbstractGrabbedData)((Sankaku)a)).ToList();
        }

        protected string AuthString(string login, SecureString pass)
        {
            byte[] array = new byte["choujin-steiner--".Length + pass.Length + "--".Length];
            Encoding.UTF8.GetBytes("choujin-steiner--").CopyTo(array, 0);

            byte[] arr = DataProtection.ReadDataFromSecureString(pass);
            for (int i = 0; i < arr.Length; i++)
            {
                array[i + "choujin-steiner--".Length] = arr[i];
            }
            DataProtection.ClearByteArray(ref arr);
            Encoding.UTF8.GetBytes("--").CopyTo(array, "choujin-steiner--".Length + pass.Length);

            loginAndPasswordHash = new Tuple<string, string>(login, BitConverter.ToString(SHA1.Create().ComputeHash(array)).Replace("-", "").ToLower());
            string result = "&login=" + loginAndPasswordHash.Item1 + "&password_hash=" + loginAndPasswordHash.Item2;

            DataProtection.ClearByteArray(ref array);

            return result;
        }

        protected override CookieContainer GetCookieContainer()
        {
            if (container == null)
                UpdateCookie();

            return container;
        }

        protected override void UpdateCookie()
        {
            HttpWebRequest hwr = WebRequest.CreateHttp("https://www.sankakucomplex.com");
            hwr.Proxy = WebRequest.DefaultWebProxy;
            hwr.UserAgent = "Other";
            hwr.Method = WebRequestMethods.Http.Get;
            hwr.CookieContainer = new CookieContainer();
            hwr.CookieContainer.Add(new Cookie("login", loginAndPasswordHash.Item1, "/", "chan.sankakucomplex.com"));
            hwr.CookieContainer.Add(new Cookie("pass_hash", loginAndPasswordHash.Item2, "/", "chan.sankakucomplex.com"));
            bool isFine = false;
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    var response = hwr.GetResponse();
                    isFine = true;
                    break;
                }
                catch (Exception)
                {
                    //repeat
                }
            }
            if (!isFine)
            {
                throw new CookieException();        //TODO: добавить кастомную ошибку
            }

            container = hwr.CookieContainer;
        }
    }
}
