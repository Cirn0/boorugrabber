﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooruGrabber.Util.GrabbedData;
using System.Net;
using System.Web.Helpers;
using System.Security.Cryptography;
using System.Windows;
using System.Threading;

namespace BooruGrabber.Util.Grabber
{
    class DeviantArtGrabber : AbstractJsonGrabber
    {
        private DateTime nextTokenUpdate = DateTime.MinValue;
        private Object accessTokenLock = new Object();
        private string _accessToken = String.Empty;
        private string accessToken
        {
            get
            {
                lock (accessTokenLock)
                {
                    if (_accessToken == String.Empty || nextTokenUpdate < DateTime.Now)
                    {
                        while (9 == 9)
                        {
                            try
                            {
                                //не смотрите на всё, что рядом со словом secret, пожалуйста
                                byte[] response = DownloadDataFromURL(@"https://www.deviantart.com/oauth2/token?grant_type=client_credentials&client_id=4562&client_secret=832bf7d2e6fccd3681b92d7cff0b8aa1");

                                string str = Encoding.UTF8.GetString(response);
                                dynamic d = Json.Decode(str);
                                if (d == null || d.Length == 0)
                                {
                                    //Не получили данные. Значит, проблемы с соединением или ещё с чем. Ждём секунду и снова пробуем.
                                    Thread.Sleep(1000);
                                    continue;
                                }
                                if (d.status == "success")
                                {
                                    _accessToken = d.access_token;
                                    nextTokenUpdate = DateTime.Now.AddSeconds(d.expires_in / 2);
                                    break;
                                }
                            }
                            catch
                            {
                                Thread.Sleep(1000);
                            }
                        }
                    }
                }
                return _accessToken;
            }
        }

        public DeviantArtGrabber()
        {
        }

        public override SimpleGrabbedFile DoConvertion(AbstractGrabbedData data)
        {
            try
            {
                DeviantArt current = (DeviantArt)data;
                SimpleGrabbedFile @return = new SimpleGrabbedFile();
                @return.id = current.GetHashCode();
                @return.hash = "";
                @return.imagePath = current.content.src;
                @return.largerPreviewPath = current.preview?.src ?? @return.imagePath;
                @return.previewPath = current.thumbs?.OrderByDescending(a => a.width)?.ToList()[0]?.src ?? @return.largerPreviewPath;
                @return.tags = current.tags;
                @return.filename = String.Concat(current.content.src.Reverse().TakeWhile(a => !a.Equals('/')).Reverse());
                @return.sourceSite = "DeviantArt";
                return @return;
            }
            catch
            {
                return null;
            }
        }

        protected override string GetURL(string tag, int pageNumber)
        {
            return String.Format(@"https://www.deviantart.com/api/v1/oauth2/browse/tags?tag={0}&access_token={1}&offset={2}", tag, accessToken, pageNumber * 10);
        }

        protected string GetTagsUrl(string[] deviationids)
        {
            string tempString = String.Empty;
            for (int i = 0; i < deviationids.Count(); i++)
            {
                tempString += String.Format("deviationids[{0}]={1}&", i, deviationids[i]);
            }
            return String.Format(@"https://www.deviantart.com/api/v1/oauth2/deviation/metadata?{0}access_token={1}", tempString, accessToken);
        }

        protected override List<AbstractGrabbedData> ProcessJsonData(byte[] json)
        {
            string str = Encoding.UTF8.GetString(json);
            dynamic d = Json.Decode(str, typeof(DeviantArt.DeviantArtMainResult));
            if (d == null) return null;
            object[] temp = (DeviantArt[])d.results;

            return temp.Select(a => (AbstractGrabbedData)((DeviantArt)a)).ToList();
        }

        protected override void ImagesListLoadingTask(object _tag)
        {
            while (9 == 9)
            {
                if (noMore)
                {
                    return;
                }
                string tag = (string)_tag;
                string url;
                lock (filesIterationIndexLock)
                {
                    url = GetURL(tag, filesIterationIndex);
                    filesIterationIndex++;
                }

                byte[] result = DownloadDataFromURL(url);

                var ret = ProcessJsonData(result);
                if (ret == null || ret.Count == 0)
                {
                    noMore = true;
                    return;
                }
                else
                {
                    string url_ = GetTagsUrl(ret.Select(a => ((DeviantArt)a).deviationid).ToArray());
                    byte[] result_ = DownloadDataFromURL(url_);
                    string str = Encoding.UTF8.GetString(result_);
                    dynamic d = Json.Decode(str, typeof(DeviantArt.DeviantArtTagsInfo));
                    ((DeviantArt.DeviantArtTagsInfo)d).metadata
                        .Select(a => new KeyValuePair<string, DeviantArt.DeviantArtTag[]>(a.deviationid, a.tags))
                        .Join(ret,
                            b => b.Key,
                            c => ((DeviantArt)c).deviationid,
                            (e, f) => new Tuple<DeviantArt, DeviantArt.DeviantArtTag[]>((DeviantArt)f, e.Value))
                        .ToList()
                        .ForEach(a => a.Item1.tags = String.Concat(a.Item2.Select(a_ => (string)a_.tag_name + " ").ToArray()));
                    int сnt;
                    lock (files)
                    {
                        сnt = ret.Except(files).Where(a => a != null).Count();
                    }
                    _imagesCount += сnt;
                    if (сnt == 0)     //может быть, что с сайта нам возвращается не пустой набор данных, а последний доступный. Для этого и проверяем
                    {
                        noMore = true;
                    }
                    lock (files)
                    {
                        files.AddRange(ret.Except(files).Where(a => a != null));
                    }
                }
            }
        }
    }
}
