﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooruGrabber.Util.GrabbedData;
using System.Net;
using System.Web.Helpers;
using System.Security.Cryptography;
using System.Windows;

namespace BooruGrabber.Util.Grabber
{
    class SafebooruGrabber: AbstractJsonGrabber
    {

        public SafebooruGrabber()
        {
            hashAlgorithm = new MD5CryptoServiceProvider();
        }

        protected override List<AbstractGrabbedData> ProcessJsonData(byte[] json)
        {
            string str = Encoding.UTF8.GetString(json);
            dynamic d = Json.Decode(str, typeof(Safebooru[]));
            if (d == null || d.Length == 0) return null;
            object[] temp = (object[])d;
            //temp?.ToList().ForEach(a => files.Add((Safebooru)a));

            //return temp.Count();
            return temp.Select(a => (AbstractGrabbedData)((Safebooru)a)).ToList();
        }

        protected override string GetURL(string tag, int pageNumber)
        {
            return "http://safebooru.org/index.php?page=dapi&s=post&q=index&tags=" + tag + "&pid=" + pageNumber.ToString() + "&json=1";
        }

        public override SimpleGrabbedFile DoConvertion(AbstractGrabbedData data)
        {
            Safebooru current = (Safebooru)data;
            SimpleGrabbedFile @return = new SimpleGrabbedFile();
            @return.id = current.id;
            @return.hash = current.hash;
            @return.imagePath = String.Format("http://safebooru.org/images/{0}/{1}", current.directory, current.image);
            @return.previewPath = (current.sample) ? String.Format("http://safebooru.org/samples/{0}/sample_{1}", current.directory, current.image) : @return.imagePath;
            @return.largerPreviewPath = @return.previewPath; ;
            @return.tags = current.tags;
            @return.filename = current.image;
            @return.sourceSite = "Safebooru";
            return @return;
        }
    }
}
