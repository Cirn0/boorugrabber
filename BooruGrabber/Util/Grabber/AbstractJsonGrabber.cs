﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooruGrabber.Util.GrabbedData;
using BooruGrabber.Util;
using System.Net;
using System.Web.Helpers;
using System.Collections.ObjectModel;
using System.Security.Cryptography;
using System.IO;
using System.Windows.Media.Imaging;
using System.Threading;

namespace BooruGrabber.Util.Grabber
{
    public abstract class AbstractJsonGrabber: AbstractGrabber
    {
        protected byte errorCounter = 0;

        protected bool TrySaveTagsInJpegMetadata(SimpleGrabbedFile currentFile, string fileName, byte[] b)
        {
            Stream sourceStream = null;
            Stream streamOut = null;
            bool result = false;
            try
            {
                BitmapCreateOptions createOptions = BitmapCreateOptions.PreservePixelFormat | BitmapCreateOptions.IgnoreColorProfile;
                BitmapDecoder sourceDecoder;
                sourceStream = new MemoryStream(b);
                sourceDecoder = BitmapDecoder.Create(sourceStream, createOptions, BitmapCacheOption.Default);
                if (sourceDecoder.Frames[0] != null && sourceDecoder.Frames[0].Metadata != null)
                {
                    BitmapMetadata sourceMetadata = sourceDecoder.Frames[0].Metadata.Clone() as BitmapMetadata;
                    //в теги нельзя вставлять символ ";"
                    sourceMetadata.Keywords = new ReadOnlyCollection<string>(currentFile.tags.Replace(';', '_').Split(' ').ToList());

                    BitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(sourceDecoder.Frames[0], sourceDecoder.Frames[0].Thumbnail, sourceMetadata, sourceDecoder.Frames[0].ColorContexts));
                    streamOut = File.Open(fileName, FileMode.Create, FileAccess.ReadWrite);
                    lock (obj)
                        encoder.Save(streamOut);
                }
                result = true;
            }
            catch (Exception)   //если уж непонятно что произошло, то попытаемся хотя бы сохранить саму картинку
            {
                //на всякий случай попытаемся закрыть его тут - потому что этот файл может быть открыт в блоке try выше
                streamOut?.Close();
                streamOut?.Dispose();
                System.IO.File.WriteAllBytes(fileName, b);
                result = false;
            }
            finally
            {
                sourceStream?.Close();
                sourceStream?.Dispose();
                streamOut?.Close();
                streamOut?.Dispose();
            }

            return result;
        }

        protected override void WriteFile(SimpleGrabbedFile currentFile, string folderPath, byte[] content)
        {
            if (IsHashFound(content))
            {
                return;
            }

            string filename = new String(currentFile.filename.TakeWhile(a => a != '?').ToArray());

            string ext = Path.GetExtension(folderPath + "\\" + filename);
            string fileName = String.Empty;

            try
            {
                fileName = GenerateFileName(folderPath, currentFile, ext);
            }
            catch (PathTooLongException)
            {
                Log.log.AddEntry(Log.MessageType.warning, String.Format("Не удаётся сохранить файл. Возможно, путь к папке для сохранения слишком длинный."));
                return;
            }


            if (DownloadingSettings.settings.saveTagsInJpegMetadata &&
                   (ext.Equals(".jpg", StringComparison.InvariantCultureIgnoreCase) ||
                    ext.Equals(".jpeg", StringComparison.InvariantCultureIgnoreCase)))
            {
                TrySaveTagsInJpegMetadata(currentFile, fileName, content);
            }
            else
            {
                File.WriteAllBytes(fileName, content);      //если не jpeg - тэги не сохраняем
            }
        }
        
        protected override void ImagesListLoadingTask(object _tag)
        {
            while (9 == 9)
            {
                if (noMore)
                {
                    return;
                }
                string tag = (string)_tag;
                string url;
                lock (filesIterationIndexLock)
                {
                    url = GetURL(tag, filesIterationIndex);
                    filesIterationIndex++;
                }

                byte[] result = DownloadingSettings.settings.useAnonymizer ? Anonymizer.LoadDataThrough(url) : DownloadDataFromURL(url);
                

                if (result == null)
                {
                    errorCounter++;
                    Log.log.AddEntry(Log.MessageType.warning, String.Format("Не удаётся получить данные по ссылке {0}.", url));

                    if (errorCounter >= 3)
                    {
                        noMore = true;
                        errorCounter = 0;
                        Log.log.AddEntry(Log.MessageType.warning, String.Format("Не удаётся получить корректные данные с сайта. Скачивание прервано."));
                    }

                    return;
                }

                List<AbstractGrabbedData> ret = null;
                try
                {
                    //может быть, что туда у нас едут не корректные данные, а заглушка от роскомпозора или ещё что, и тогда распарсить не удастся
                    ret = ProcessJsonData(result);
                    errorCounter = 0;
                }
                catch
                {
                    errorCounter++;
                    Log.log.AddEntry(Log.MessageType.warning, String.Format("Получены некорректные данные по ссылке {0}.", url));

                    if (errorCounter >= 3)
                    {
                        noMore = true;
                        errorCounter = 0;
                        Log.log.AddEntry(Log.MessageType.warning, String.Format("Не удаётся получить корректные данные с сайта. Скачивание прервано."));
                    }

                    return;
                }
                if (ret == null || ret.Count == 0)
                {
                    noMore = true;
                    return;
                }
                else
                {
                    int сnt;
                    lock (files)
                    {
                        сnt = ret.Except(files).Where(a => a != null).Count();
                    }
                    _imagesCount += сnt;
                    if (сnt == 0)     //может быть, что с сайта нам возвращается не пустой набор данных, а последний доступный. Для этого и проверяем
                    {
                        noMore = true;
                    }
                    lock (files)
                    {
                        files.AddRange(ret.Except(files).Where(a => a != null));
                    }
                }
            }
        }

        protected abstract string GetURL(string tag, int pageNumber);

        protected abstract List<AbstractGrabbedData> ProcessJsonData(byte[] json);

        protected override bool CompareHash(byte[] filesData, string hash)
        {
            if (hashAlgorithm == null)
                return true;        //если алгоритм хэширования не задан, то считаем, что проверка на целостность не требуется
            string calculatedHash = BitConverter.ToString(hashAlgorithm.ComputeHash(filesData)).Replace("-", "");
            return calculatedHash.Equals(hash, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
