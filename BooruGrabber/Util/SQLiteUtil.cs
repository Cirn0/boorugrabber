﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BooruGrabber.Util.GrabbedData;
using BooruGrabber.Util.Grabber;

namespace BooruGrabber.Util
{
    public static class SQLiteUtil
    {
        private const string dataBaseFileName = "info.db3";

        //скорее всего, не пригодится - при скачивании списка мы его получаем автоматически только в виде List<SimpleGrabbedFile>. Но хотелось бы его завести, чтобы сохранять данные более структурированно, так что пока оставлю как коммент
        //public static void WriteListIntoDatabase(List<AbstractGrabbedData> data, string path, bool partialSaving = true)
        //{
        //    if (partialSaving)
        //    {
        //        //WriteListIntoDatabase(data.Select(a => grabber.DoConvertion(a)).ToList(), path);
        //    }
        //    else
        //    {
        //        //TODO
        //    }
        //}

        public static void WriteListIntoDatabase(List<SimpleGrabbedFile> data, string path)
        {
            SQLiteConnection conn;
            if (GetConnection(path, out conn))
            {
                CreateSimpleTables(conn);
            }
            SQLiteTransaction transaction = conn.BeginTransaction();
            data.ForEach(a =>
            {
                using (SQLiteCommand comma = new SQLiteCommand(conn))
                {
                    comma.Transaction = transaction;
                    comma.CommandText = @"insert or ignore into images (id, sourceSite, imagePath, largerPreviewPath, previewPath, tags, hash, filename) 
                                          values (@id, @sourceSite, @imagePath, @largerPreviewPath, @previewPath, @tags, @hash, @filename)";
                    comma.Parameters.Add(new SQLiteParameter("@id", a.id));
                    comma.Parameters.Add(new SQLiteParameter("@sourceSite", a.sourceSite));
                    comma.Parameters.Add(new SQLiteParameter("@imagePath", a.imagePath));
                    comma.Parameters.Add(new SQLiteParameter("@largerPreviewPath", a.largerPreviewPath));
                    comma.Parameters.Add(new SQLiteParameter("@previewPath", a.previewPath));
                    comma.Parameters.Add(new SQLiteParameter("@tags", a.tags));
                    comma.Parameters.Add(new SQLiteParameter("@hash", a.hash));
                    comma.Parameters.Add(new SQLiteParameter("@filename", a.filename));
                    comma.CommandType = CommandType.Text;
                    comma.ExecuteNonQuery();
                }
            });
            transaction.Commit();
            transaction.Dispose();
        }

        public static SimpleGrabbedFile[] ReadListFromDatabase(string file)
        {
            List<SimpleGrabbedFile> result = new List<SimpleGrabbedFile>();

            SQLiteConnection conn;
            bool isNew = GetConnection(file, out conn);
            if (isNew)
            {
                return null;
            }

            using (SQLiteCommand comma = new SQLiteCommand(conn))
            {
                comma.CommandText = @"select id, sourceSite, imagePath, largerPreviewPath, previewPath, tags, hash, filename from images";
                try
                {
                    using (var reader = comma.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(new SimpleGrabbedFile()
                            {
                                filename = (string)reader["filename"],
                                hash = (string)reader["hash"],
                                id = (int)reader["id"],
                                imagePath = (string)reader["imagePath"],
                                largerPreviewPath = (string)reader["largerPreviewPath"],
                                previewPath = (string)reader["previewPath"],
                                sourceSite = (string)reader["sourceSite"],
                                tags = (string)reader["tags"]
                            });
                        }
                    }
                    return result.ToArray();
                }
                catch
                {
                    return null;
                }
            }
        }

        private static bool GetConnection(string path, out SQLiteConnection conn)
        {
            bool isNew = false;
            //в случае создания новой базы мы сюда передаём только папку, в которой будем создавать базу. А если открываем существующую - то тут уже с именем. Вот тут и проверяем, надо ли дописывать дефолтное имя файла или так сойдёт
            if ((File.Exists(path) || Directory.Exists(path)) && File.GetAttributes(path).HasFlag(FileAttributes.Directory))
            {
                path = path + "\\" + dataBaseFileName;
            }
            SQLiteFactory factory = (SQLiteFactory)DbProviderFactories.GetFactory("System.Data.SQLite");
            if (!File.Exists(path))
            {
                SQLiteConnection.CreateFile(path);
                isNew = true;
            }
            conn = (SQLiteConnection)factory.CreateConnection();
            conn.ConnectionString = "Data Source = " + path;
            conn.Open();

            return isNew;
        }

        private static void CreateSimpleTables(SQLiteConnection conn)
        {
            using (SQLiteCommand comma = new SQLiteCommand(conn))
            {
                //поставил в качестве уникального значения путь к картинке, потому что я до сих пор не верю в то, что невозможна коллизия хэшей
                comma.CommandText = @"CREATE TABLE [images] (
                                      [databaseID] integer PRIMARY KEY AUTOINCREMENT NOT NULL,
                                      [ID] int NOT NULL,
                                      [sourceSite] char(100) NOT NULL,
                                      [imagePath] char(1000) NOT NULL UNIQUE,
                                      [largerPreviewPath] char(1000) NOT NULL,
                                      [previewPath] char(1000) NOT NULL,
                                      [tags] char(1000) NOT NULL,
                                      [hash] char(64) NULL,
                                      [filename] char(255) NOT NULL
                                      );";
                comma.CommandType = CommandType.Text;
                comma.ExecuteNonQuery();
            }
        }
    }
}
