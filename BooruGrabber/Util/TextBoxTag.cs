﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooruGrabber.Util
{
    class TextBoxTag
    {
        public bool isEmpty;
        public string valueWhenEmpty;

        public TextBoxTag(bool isEmpty, string valueWhenEmpty)
        {
            this.isEmpty = isEmpty;
            this.valueWhenEmpty = valueWhenEmpty;
        }
    }
}
