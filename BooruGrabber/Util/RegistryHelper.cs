﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Windows.Shapes;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace BooruGrabber.Util
{
    public static class RegistryHelper
    {
        private static RegistryKey credentialsKey;
        private static RegistryKey settingsKey;

        static RegistryHelper()
        {
            settingsKey = Registry.CurrentUser.OpenSubKey("Software\\BooruGrabber", true);
            if (settingsKey == null)
            {
                Registry.CurrentUser.CreateSubKey("Software\\BooruGrabber");
                settingsKey = Registry.CurrentUser.OpenSubKey("Software\\BooruGrabber", true);
            }
            credentialsKey = Registry.CurrentUser.OpenSubKey("Software\\BooruGrabber\\StoredCredentials", true);
            if (credentialsKey == null)
            {
                Registry.CurrentUser.CreateSubKey("Software\\BooruGrabber\\StoredCredentials");
                credentialsKey = Registry.CurrentUser.OpenSubKey("Software\\BooruGrabber\\StoredCredentials", true);
            }
        }

        public static void WriteCredentialValue(string name, string value)
        {
            try
            {
                credentialsKey.DeleteValue(name);
            }
            catch { }
            credentialsKey.SetValue(name, value, RegistryValueKind.String);
        }

        public static string ReadCredentialValue(string name)
        {
            object o = credentialsKey.GetValue(name);
            if (o == null)
            {
                return String.Empty;
            }
            return (string)o;
        }

        public static List<string> EnumerateCredentialValues()
        {
            return new List<string>(credentialsKey.GetValueNames());
        }

        public static int ReadSettingsValue()
        {
            object o = settingsKey.GetValue("downloadingSettings");
            if (o == null)
            {
                return 6;
            }
            return (int)o;
        }

        public static void WriteSettingsValue(int value)
        {
            try
            {
                settingsKey.DeleteValue("downloadingSettings");
            }
            catch { }
            settingsKey.SetValue("downloadingSettings", value, RegistryValueKind.DWord);
        }

        public static WindowState ReadWindowStateValue(string windowTag)
        {
            object o = settingsKey.GetValue(windowTag);
            if (o == null)
            {
                return null;
            }
            byte[] arr = (byte[])o;
            using (var ms = new MemoryStream(arr))
            {
                BinaryFormatter form = new BinaryFormatter();
                return (WindowState)form.Deserialize(ms);
                //return (WindowState)o;
            }
        }

        public static void WriteWindowState(string windowTag, WindowState state)
        {
            using (var ms = new MemoryStream())
            {
                BinaryFormatter form = new BinaryFormatter();
                form.Serialize(ms, state);
                byte[] arr = ms.ToArray();
                try
                {
                    settingsKey.DeleteValue(windowTag);
                }
                catch { }
                settingsKey.SetValue(windowTag, arr, RegistryValueKind.Binary);
            }
        }

        [Serializable]
        public class WindowState
        {
            public double width;
            public double height;
            public double left;
            public double top;
        }
    }
}
