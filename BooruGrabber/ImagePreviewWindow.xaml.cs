﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BooruGrabber.Util.GrabbedData;
using BooruGrabber.Util.Grabber;
using BooruGrabber.Util;

namespace BooruGrabber
{
    public partial class ImagePreviewWindow : Window, IDisposable
    {
        System.IO.Stream ms;

        public ImagePreviewWindow(Image image_, AbstractGrabber grabber)
        {
            try
            {
                InitializeComponent();
                SimpleGrabbedFile file = (SimpleGrabbedFile)image_.Tag;
                byte[] previewContent = grabber.DownloadLargerPreview(file);
                ms = new System.IO.MemoryStream(previewContent);
                BitmapImage img = new BitmapImage();
                img.BeginInit();
                img.StreamSource = ms;
                img.EndInit();
                //end

                this.Width = (img.PixelWidth > 800) ? img.PixelWidth : 800;
                double intermedHeight = this.Width * img.PixelHeight / img.PixelWidth;
                if (intermedHeight > 600)
                {
                    this.Width *= 600 / intermedHeight;
                    this.Height = 600;
                }
                else
                {
                    this.Height = intermedHeight;
                }

                image.Source = img;
            }
            catch
            {
                Log.log.AddEntry(Log.MessageType.warning, String.Format("Для данного файла предпросмотр не поддерживается."));
            }
        }

        public void Dispose()
        {
            ms.Dispose();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            RegistryHelper.WriteWindowState("imagePreview", new RegistryHelper.WindowState() { height = 0, left = this.Left, top = this.Top, width = 0 });
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            var v = RegistryHelper.ReadWindowStateValue("imagePreview");
            if (v == null)
            {
                return;
            }
            this.Left = v.left;
            this.Top = v.top;
        }
    }
}
