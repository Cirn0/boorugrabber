﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Net;
using BooruGrabber.Util.GrabbedData;
using BooruGrabber.Util.Grabber;
using BooruGrabber.Util;
using System.IO;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Data.SQLite;

namespace BooruGrabber
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("kernel32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetProcessWorkingSetSize(IntPtr process,
        UIntPtr minimumWorkingSetSize, UIntPtr maximumWorkingSetSize);

        private AbstractGrabber grabber;
        private bool currentlyGrabbing = false;
        private bool currentlyDownloading = false;
        ObservableCollection<SimpleGrabbedFile> files;

        Thickness normalDataGridMargin;
        private object previousSelectedTab;

        public MainWindow()
        {
            InitializeComponent();
            textBox.Tag = new TextBoxTag(true, "Tags...");
            textBox1.Tag = new TextBoxTag(true, "Path to save folder...");
            textBox_LostFocus(textBox, null);
            textBox_LostFocus(textBox1, null);

            ServicePointManager.DefaultConnectionLimit = Int32.MaxValue;        //ну а чё он?
        }

        private async void button_Click(object sender, RoutedEventArgs e)
        {
            if (currentlyGrabbing)
            {
                grabber.AbortCurrentOperation();
                return;
            }

            button.Content = "Abort";
            //button.IsEnabled = false;
            currentlyGrabbing = true;
            AbstractGrabber ag = null;
            try
            {
                ag = GrabberUtil.GetGrabberBySiteName(((ComboBoxItem)comboBox.SelectedValue).Tag?.ToString());
            }
            catch
            {
                Log.log.AddEntry(Log.MessageType.warning, String.Format("Ошибка при определении сайта-источника для изображений."));
            }
            if (ag == null)
            {
                button.IsEnabled = true;
                return;
            }
            grabber = ag;
            string correctedTags = TagCorrector.CorrectLink(textBox.Text);
            Task<ObservableCollection<SimpleGrabbedFile>> task = null;
            try
            {
                task = grabber.GetImagesList(correctedTags, DownloadingSettings.settings.threadsCount);
                progressBar.IsIndeterminate = true;

                while (!task.IsCompleted)
                {
                    await Task.Delay(250);
                    imageCounter.Text = "Получено изображений: " + grabber.imagesCount.ToString();
                }
            }
            catch
            {
                Log.log.AddEntry(Log.MessageType.warning, String.Format("Возникла необработанная ошибка при получении списка изображений с сайта."));
            }
            finally
            {
                progressBar.IsIndeterminate = false;
                //button.IsEnabled = true;
                button.Content = "Grab";
                currentlyGrabbing = false;
                if (files == null)
                {
                    files = task?.Result;
                }
                else
                {
                    var v = files.Select(a => (a.hash != String.Empty) ? a.hash : a.imagePath);     //для хтмл-картинок нету хэшей, поэтому будем ориентироваться по именам файла
                    task?.Result.Where(a => !v.Contains(a.hash)).ToList().ForEach(a => files.Add(a));
                }
            }

            if (files == null)
            {
                return;
            }
            
            dataGrid.ItemsSource = files;
            progressBar2.Maximum = files.Count;
            imageCounter.Text = "Получено изображений: " + files.Count.ToString();
        }

        

        private void textBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (!(sender is TextBox)) return;
            TextBox currentTextBox = sender as TextBox;
            if (((TextBoxTag)currentTextBox.Tag).isEmpty)
            {
                currentTextBox.Text = "";
            }
            currentTextBox.FontStyle = FontStyles.Normal;
            currentTextBox.Foreground = Brushes.Black;
        }

        private void textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!(sender is TextBox)) return;
            TextBox currentTextBox = sender as TextBox;
            if (currentTextBox.Text == String.Empty)
            {
                currentTextBox.Text = ((TextBoxTag)currentTextBox.Tag).valueWhenEmpty;
                currentTextBox.FontStyle = FontStyles.Italic;
                currentTextBox.Foreground = Brushes.Gray;
                ((TextBoxTag)currentTextBox.Tag).isEmpty = true;
            }
            else
            {
                ((TextBoxTag)currentTextBox.Tag).isEmpty = false;
            }
        }

        [SuppressMessage("ReSharper", "PossibleNullReferenceException")]        //потому что в этом случае мне и надо, чтобы была ошибка
        private async void dataGrid_RowDetailsVisibilityChanged(object sender, DataGridRowDetailsEventArgs e)
        {
            try
            {
                Image img = e.DetailsElement.FindName("previewImage") as Image;
                img.Source = null;
                dynamic d = e.Row.DataContext;
                img.Tag = d;

                if (e.DetailsElement.IsVisible)
                {
                    byte[] previewContent = await grabber.DownloadPreview(d);
                    var ms = new System.IO.MemoryStream(previewContent);
                    BitmapImage im = new BitmapImage();
                    im.BeginInit();
                    im.StreamSource = ms;
                    im.EndInit();
                    img.Unloaded += (_, __) =>
                    {
                        ms.Close();
                        ms.Dispose();
                    };

                    img.Source = im;
                    img.Width = im.Width * img.Height / im.Height;
                }
                else
                {
                    img.Source = null;
                }
            }
            catch
            {
                Log.log.AddEntry(Log.MessageType.warning, String.Format("Ошибка при загрузке превью."));
            }
        }

        private async void button2_Click(object sender, RoutedEventArgs e)
        {
            if (currentlyDownloading)
            {
                grabber.AbortCurrentOperation();
                return;
            }

            if (Uri.IsWellFormedUriString(textBox1.Text, UriKind.Absolute))
            {
                MessageBox.Show("Некорректная папка для сохранения файлов.");
                return;
            }

            currentlyDownloading = true;
            //button2.IsEnabled = false;
            button2.Content = "Abort";
            button.IsEnabled = false;

            progressBar2.Maximum = ((dataGrid.SelectedItems.Count != 0) ? dataGrid.SelectedItems.Count : files.Count);
            taskBarItemInfo1.ProgressState = System.Windows.Shell.TaskbarItemProgressState.Normal;
            taskBarItemInfo1.ProgressValue = 0;
            int alreadyDownloaded = 0;

            foreach (var site in files.Select(a => a.sourceSite).Distinct().ToList())
            {
                grabber = GrabberUtil.GetGrabberBySiteName(site);

                List<SimpleGrabbedFile> fls = new List<SimpleGrabbedFile>();
                if (dataGrid.SelectedItems.Count != 0)
                {
                    dataGrid.SelectedItems.Cast<SimpleGrabbedFile>().Where(a => a.sourceSite.Equals(site)).ToList().ForEach(a => fls.Add(a));
                }
                else
                {
                    files.Where(a => a.sourceSite.Equals(site)).ToList().ForEach(a => fls.Add(a));
                }

                if (DownloadingSettings.settings.dontDownloadDoubles)
                {
                    try
                    {
                        imageCounter.Text = "Идёт хэширование файлов...";
                        progressBar2.IsIndeterminate = true;
                        await GrabberUtil.SkipDoubles(textBox1.Text, fls, grabber.hashAlgorithm);
                        imageCounter.Text = "";
                        progressBar2.IsIndeterminate = false;
                    }
                    catch
                    {
                        Log.log.AddEntry(Log.MessageType.warning, String.Format("Ошибка при анализе файлов в выбранной папке."));
                    }
                }

                ObservableCollection<SimpleGrabbedFile> tempo = new ObservableCollection<SimpleGrabbedFile>();
                fls.ForEach(a => tempo.Add(a));
                grabber.AssignObservableCollection(ref tempo);

                try
                {
                    Task t = grabber.DownloadImages(textBox1.Text, DownloadingSettings.settings.threadsCount);

                    while (!t.IsCompleted)
                    {
                        await Task.Delay(250);
                        progressBar2.Value = alreadyDownloaded + grabber.downloadedCount;
                        taskBarItemInfo1.ProgressValue = (progressBar2.Value / progressBar2.Maximum);
                        if (grabber.currentlyProcessedFile != null)
                            dataGrid.ScrollIntoView(grabber.currentlyProcessedFile);
                    }
                }
                catch
                {
                    Log.log.AddEntry(Log.MessageType.warning, String.Format("Ошибка при скачивании файлов."));
                }

                alreadyDownloaded += grabber.downloadedCount;
            }

            button2.Content = "Download";
            currentlyDownloading = false;
            button.IsEnabled = true;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                DependencyObject dep = (DependencyObject)sender;

                // iteratively traverse the visual tree
                while ((dep != null) &
                !(dep is DataGridCell))
                {
                    dep = VisualTreeHelper.GetParent(dep);
                }

                if (dep == null)
                    return;

                if (dep is DataGridCell)
                {
                    DataGridCell cell = dep as DataGridCell;

                    while ((dep != null) & !(dep is DataGridRow))
                    {
                        dep = VisualTreeHelper.GetParent(dep);
                    }

                    DataGridRow row = dep as DataGridRow;

                    row.DetailsVisibility = row.DetailsVisibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;

                    string content = ((Button)sender).Content.ToString();
                    if (content.Equals("+"))
                    {
                        ((Button)sender).Content = "-";
                    }
                    else
                    {
                        ((Button)sender).Content = "+";
                    }
                }
            }
            catch
            {
                Log.log.AddEntry(Log.MessageType.warning, String.Format("Внутренняя ошибка приложения при попытке показать маленькое превью."));
            }
        }

        private void previewImage_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                using (var v = new ImagePreviewWindow((Image)sender, grabber))
                {
                    v.Show();
                }
            }
            catch
            {
                Log.log.AddEntry(Log.MessageType.warning, String.Format("Ошибка при попытке показать большое превью."));
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var v = new Microsoft.WindowsAPICodePack.Dialogs.CommonOpenFileDialog();
            v.IsFolderPicker = true;
            if (v.ShowDialog() == Microsoft.WindowsAPICodePack.Dialogs.CommonFileDialogResult.Ok)
            {
                textBox1.Text = v.FileName;
                ((TextBoxTag)textBox1.Tag).isEmpty = false;
                textBox_GotFocus(textBox1, null);
            }
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            var v = new Microsoft.WindowsAPICodePack.Dialogs.CommonSaveFileDialog();
            if (DownloadingSettings.settings.currentListSavingMethod == DownloadingSettings.ListSavingMethods.SQLite)
            {
                v.DefaultExtension = "db3";
                
            }
            if (v.ShowDialog() == Microsoft.WindowsAPICodePack.Dialogs.CommonFileDialogResult.Ok)
            {
                try
                {
                    if (DownloadingSettings.settings.currentListSavingMethod == DownloadingSettings.ListSavingMethods.SQLite)
                    {
                        SQLiteUtil.WriteListIntoDatabase(files.ToList(), v.FileName);
                    }
                    else
                    {
                        System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
                        js.MaxJsonLength = Int32.MaxValue;
                        System.IO.File.WriteAllText(v.FileName, js.Serialize(files));
                    }
                }
                catch
                {
                    Log.log.AddEntry(Log.MessageType.warning, String.Format("Ошибка при сохранении списка изображений в файл."));
                }
            }
        }

        private void button3_Copy_Click(object sender, RoutedEventArgs e)
        {
            var v = new Microsoft.WindowsAPICodePack.Dialogs.CommonOpenFileDialog();
            v.EnsureFileExists = true;
            v.Multiselect = false;
            if (v.ShowDialog() == Microsoft.WindowsAPICodePack.Dialogs.CommonFileDialogResult.Ok)
            {
                try
                {
                    FileInfo fn = new FileInfo(v.FileName);
                    object[] temp;
                    if (fn.Extension.Equals(".db3", StringComparison.InvariantCultureIgnoreCase))
                    {
                        temp = SQLiteUtil.ReadListFromDatabase(v.FileName);
                    }
                    else
                    {
                        string b = File.ReadAllText(v.FileName);
                        System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
                        js.MaxJsonLength = Int32.MaxValue;
                        dynamic d = js.Deserialize(b, typeof(SimpleGrabbedFile[]));
                        //dynamic d = Json.Decode(b, typeof(SimpleGrabbedFile[]));
                        if (d == null || d.Length == null)
                        {
                            Log.log.AddEntry(Log.MessageType.warning, String.Format("Ошибка: некорректный входной файл."));
                            return;
                        }
                        temp = (object[])d;
                    }
                    files = new ObservableCollection<SimpleGrabbedFile>();
                    temp.ToList().ForEach(a => files.Add((SimpleGrabbedFile)a));
                    imageCounter.Text = "Получено изображений: " + files.Count;
                    grabber = GrabberUtil.GetGrabberBySiteName(files[0]?.sourceSite);
                    dataGrid.ItemsSource = files;
                    progressBar2.Maximum = files.Count;
                }
                catch
                {
                    Log.log.AddEntry(Log.MessageType.warning, String.Format("Ошибка при загрузке списка изображений из файла."));
                }
            }
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            HttpWebRequest hwr = WebRequest.CreateHttp("http://txt.proxyspy.net/proxy.txt");
            hwr.Proxy = WebRequest.DefaultWebProxy;
            hwr.UserAgent = "Other";
            try
            {
                ProxyHelper.ProxyList.LoadProxies(hwr.GetResponse().GetResponseStream());
                //MessageBox.Show(ProxyHelper.ProxyList.LoadProxies(new System.IO.StreamReader("E:\\123\\321\\proxy.txt").BaseStream).ToString());
            }
            catch (Exception ex)
            {
                Log.log.AddEntry(Log.MessageType.warning, String.Format("Ошибка при попытке загрузить прокси. \r\n" + ex.Message));
            }
            
        }

        private void button5_Click(object sender, RoutedEventArgs e)
        {
            //Log.log.AddEntry(Log.MessageType.notice, "test");
            //GC.Collect(GC.MaxGeneration);
            //GC.WaitForPendingFinalizers();
            //SetProcessWorkingSetSize(Process.GetCurrentProcess().Handle,
            //    (UIntPtr)0xFFFFFFFF, (UIntPtr)0xFFFFFFFF);
            //Anonymizer.LoadDataThrough("http://konachan.com/post.json?page=1&tags=shiina_mayuri");
            //Anonymizer.LoadDataThrough("https://files.yande.re/image/bedda0319dcad8b9860fc971740d55c2/yande.re%20269629%20hashida_itaru%20kiryu_moeka%20makise_kurisu%20megane%20nineo%20okabe_rintarou%20pantyhose%20shiina_mayuri%20steins%3Bgate.jpg");
            
            SQLiteUtil.WriteListIntoDatabase(files.ToList(), "E:\\123\\321");
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            RegistryHelper.WriteWindowState("main", new RegistryHelper.WindowState() { height = this.Height, left = this.Left, top = this.Top, width = this.Width });
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            var v = RegistryHelper.ReadWindowStateValue("main");
            if (v == null)
            {
                return;
            }
            this.Height = v.height;
            this.Width = v.width;
            this.Left = v.left;
            this.Top = v.top;
        }

        private void logTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!logTextBox.IsFocused)
            {
                logTextBox.ScrollToEnd();
            }
        }

        private void hideLog_Click(object sender, RoutedEventArgs e)
        {
            if (logTextBox.Visibility == Visibility.Visible)
            {
                logTextBox.Visibility = Visibility.Collapsed;
                var v = dataGrid.Margin;
                normalDataGridMargin = v;
                dataGrid.Margin = new Thickness(v.Left, v.Top, v.Right, logTextBox.Margin.Bottom);
                hideLog.Content = "Show log";
            }
            else
            {
                logTextBox.Visibility = Visibility.Visible;
                dataGrid.Margin = normalDataGridMargin;
                hideLog.Content = "Hide log";
            }
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedImageCounter.Text = String.Format("Выбрано строк: {0}", dataGrid.SelectedItems.Count);
        }

        private void ProgressBar_Loaded(object sender, RoutedEventArgs e)
        {
            //оказывается, каким-то хреном в WPF анимация прогресс-бара (где по нему непрерывно двигается такая зелёная хреновинка) жрёт проц, причём нещадно. Два таких прогресс-бара отжирали 3-6% (на Core i3). Слов нет, только матерные.
            //поэтому используем вот такой вот костыль, который прячет нахер эту анимацию (использовать только для тех прогрессбаров, у которых IsIndeterminate == false, иначе по нему вообще не будет видно, что он работает)
            var glow = (sender as ProgressBar)?.Template.FindName("PART_GlowRect", (ProgressBar)sender) as FrameworkElement;
            if (glow != null) glow.Visibility = Visibility.Collapsed;
        }

        private void settingsTab_GotFocus(object sender, RoutedEventArgs e)
        {
            tabControl.SelectedItem = previousSelectedTab ?? tabControl.Items[0];
            new SettingsForm().ShowDialog();
            e.Handled = true;
        }

        private void tabControl_ElementGotFocus(object sender, RoutedEventArgs e)
        {
            previousSelectedTab = tabControl.SelectedItem;
        }
    }
}